#assemble boot.s file
as --32 bin/boot.s -o bin/boot.o
#compile kernel.c file

#compiling all libraries (drivers and etc..) first to object files and then the main kernell
nasm -f elf32 -o bin/interrupt.o ./src/kernel/Descriptors/idt.s 
nasm -f elf32 -o bin/gdt.o ./src/kernel/Descriptors/gdt.s 
gcc -m32 -c src/kernel/Userspace/Syscalls/Syscalls.c -o bin/Syscalls.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
gcc -m32 -c src/kernel/MultiThreading/Time/Time.c -o bin/Time.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
echo #########################################################################
gcc -m32 -c src/kernel/Userspace/Bridge/Bridge.c -o bin/Bridge.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
echo #########################################################################
gcc -m32 -c src/kernel/Helpers/Vectors/uint32/VectorInt.c -o bin/vecint.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
gcc -m32 -c src/kernel/Helpers/Vectors/String/String.c -o bin/string.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
gcc -m32 -c src/kernel/Descriptors/descriptor_table.c -o bin/descriptor_table.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
gcc -m32 -c src/kernel/Userspace/Terminal/assistant.c -o bin/assistant.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
gcc -m32 -c src/kernel/Userspace/Terminal/terminal.c -o bin/terminal.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
gcc -m32 -c src/kernel/Helpers/types.c -o bin/types.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
gcc -m32 -c src/kernel/Helpers/helper.c -o bin/helper.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
gcc -m32 -c src/kernel/Descriptors/isr.c -o bin/isr.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
#gcc -m32 -c src/kernel/ordered_array.c -o bin/ordered_array.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
gcc -m32 -c src/kernel/Memory/paging.c -o bin/paging.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
gcc -m32 -c src/kernel/Memory/heap_allocation.c -o bin/heap_allocation.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
##
gcc -m32 -c src/kernel/FileSystem/FileSystem.c -o bin/FileSystem.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
gcc -m32 -c src/kernel/drivers/keyboard/keyboard_driver.c -o bin/keyboard.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
gcc -m32 -c src/kernel/drivers/screen/screen_driver.c -o bin/screen.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
gcc -m32 -c src/kernel/kernel.c -o bin/kernel.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
#linking the drivers, kernell and etc files (compiled object files) to a binary file (ELF)
ld -m elf_i386 -T bin/linker.ld bin/kernel.o bin/gdt.o bin/string.o bin/Bridge.o bin/Time.o bin/keyboard.o bin/interrupt.o bin/descriptor_table.o bin/screen.o bin/helper.o bin/paging.o bin/heap_allocation.o bin/types.o bin/terminal.o bin/assistant.o bin/Syscalls.o bin/isr.o bin/FileSystem.o bin/vecint.o bin/boot.o -o bin/BALI_BINUX.bin -nostdlib
#check BALI_BINUX.bin file is x86 multiboot file or not
grub-file --is-x86-multiboot bin/BALI_BINUX.bin
#building the iso file for the emulator/pc
mkdir -p isodir/boot/grub
cp bin/BALI_BINUX.bin isodir/boot/BALI_BINUX.bin
cp bin/grub.cfg isodir/boot/grub/grub.cfg
grub-mkrescue -o bin/BALI_BINUX.iso isodir
#run it in qemu (emulator)
#chown root:kvm /dev/kvm
#qemu-system-x86_64 -m 512 -machine ubuntu,accel=kvm -cdrom bin/BALI_BINUX.iso
qemu-system-x86_64 -cdrom bin/BALI_BINUX.iso
echo end