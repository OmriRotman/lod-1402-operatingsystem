#ifndef ISR_H
#define ISR_H

#define DIVISON_BY_ZERO 0
#define INVALID_OPCODE 6
#define PAGE_FAULT 14
#define SYSCALL_INTERRUPT 19

#include "../drivers/keyboard/keyboard_driver.h"
#include "../drivers/screen/screen_driver.h"
#include "../Memory/heap_allocation.h"

typedef struct registers
{
    uint32 ds;                  // Data segment selector
    uint32 edi, esi, ebp, esp, ebx, edx, ecx, eax; // Pushed by pusha.
    uint32 int_no, err_code;    // Interrupt number and error code (if applicable)
    uint32 eip, cs, eflags, useresp, ss; // Pushed by the processor automatically.
} registers_t;

typedef struct Syscall_params
{
    uint32 syscall_id;
    uint32* first;
    uint32* second;
    uint32* third;
    uint32* fourth;
    uint32* fifth;
}SysParms;

typedef void (*isr_t)(registers_t);
void isr_handler(registers_t regs);
void register_syscall_handler(uint8 n, isr_t handler);
void syscall_handler(registers_t *regs);
void initialize_syscall_params();
void set_syscall_parameters(uint32 sysID,uint32* first,uint32* second,uint32* third,uint32* fourth,uint32* fifth);
SysParms* get_syscall_parameters();

#endif