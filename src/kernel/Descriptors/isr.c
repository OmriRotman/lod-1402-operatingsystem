#include "isr.h"

isr_t interrupt_handlers[256]; //this is the interrupt vector table,basically an array of pointers to functions

static SysParms* static_syscall_parameters; //static pointer that will hold the parameters for the syscalls
// This gets called from our ASM interrupt handler stub.
void isr_handler(registers_t regs)
{
    if(regs.int_no == PAGE_FAULT)
    {
        uint32 faulting_address;
        asm volatile("mov %%cr2, %0" : "=r" (faulting_address)); //getting the problematic address from the control register
                
        // searching for the error caused the page fault by looking in the erorr code's flags
        int present   = !(regs.err_code & 0x1); 
        int rw = regs.err_code & 0x2;      
        int us = regs.err_code & 0x4;        
        int reserved = regs.err_code & 0x8;    
        int id = regs.err_code & 0x10;

        print_str("Page fault!( ");
        if (present)  //not using else if because multiple error causes are also possible
        {
            print_str("present ");
        }
        if (rw) 
        {
            print_str("read-only ");
        }
        if (us) 
        {
            print_str("user-mode ");
        }
        if (reserved) 
        {
            print_str("reserved ");
        }
        print_str(") at 0x");
        print_hex(faulting_address);
        print_str("\n");
        while (true) //stop the code from running!
        {
        }
    }
    else if(regs.int_no == 6)
    {
        print_str("System Error : Invalid opcode!\n");
        while (true)
        {
        }
        
    }
    else if(regs.int_no == 0)
    {
        print_str("System Error : Can not divide by 0!\n");
    }
    else
    {
        syscall_handler(&regs);
    }
}

void register_syscall_handler(uint8 n, isr_t handler)
{
    print_str("System info : Registering ");
    print_unsigned_int(n);
    print_str(" interrupt handler...\n");
    interrupt_handlers[n] = handler;
}

void syscall_handler(registers_t *regs)
{
    if (interrupt_handlers[regs->int_no] != 0) //checking the interrupt vector table in the address of the syscall number to see if it was addes
    {
        isr_t handler = interrupt_handlers[regs->int_no];
        handler(*regs); //calling the function
    }
    else
    {
        print_str("Error : Interrupt ");
        print_hex(regs->int_no);
        print_str(" does not exist.\n");
    }
}

/*
this function initializes the syscall parameters by allocing a sys params struct in the given static pointer
input:none
output:none
*/
void initialize_syscall_params()
{
    static_syscall_parameters = (SysParms*)alloc(sizeof(SysParms));
}

/*
this function sets the syscall parameters so I can use it before launching the interrupt
input:pointers to parameters and syscall id
output:none
*/
void set_syscall_parameters(uint32 sysID,uint32* first,uint32* second,uint32* third,uint32* fourth,uint32* fifth)
{
    static_syscall_parameters->syscall_id = sysID;
    static_syscall_parameters->first = first;
    static_syscall_parameters->second = second;
    static_syscall_parameters->third = third;
    static_syscall_parameters->fourth = fourth;
    static_syscall_parameters->fifth = fifth;
}

/*
this function gets the syscall parameters struct so i can read it once the syscall interrupt fires
input:none
output: pointer to the params struct (SysParms*)
*/
SysParms* get_syscall_parameters()
{
    return static_syscall_parameters;
}