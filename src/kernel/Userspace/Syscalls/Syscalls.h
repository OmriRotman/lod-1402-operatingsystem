#ifndef SYSCALLS_H
#define SYSCALLS_H

#include "../../Descriptors/isr.h"
#include "../../Helpers/helper.h"
#include "../../Helpers/types.h"
#include "../../FileSystem/FileSystem.h"
#include "../../Memory/paging.h"
#include "../../Memory/heap_allocation.h"
#include "../../drivers/keyboard/keyboard_driver.h"
#include "../../drivers/screen/screen_driver.h"

void init_syscalls_handlers();
void handle_syscalls(registers_t *registers);

#endif