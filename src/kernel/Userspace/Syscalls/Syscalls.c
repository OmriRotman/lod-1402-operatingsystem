#include "Syscalls.h"

/*
this function initializes syscall handlers by initiallizing an interrupt handler to 19d and points it to the syscall handler func
input:none
output:none
*/
void init_syscalls_handlers() //linking interrupt number 19 or 0x13 to the syscall handling
{
    print_str("System info : Initializing Syscalls...\n");
    register_syscall_handler(SYSCALL_INTERRUPT,&handle_syscalls);
}

/*
this function is the syscall handler, it gets syscall number value and registers and calls the fitting syscall
input:registers (sysID num and parameters included)
output:none
*/
void handle_syscalls(registers_t *registers)
{
    //print_str("Choosing syscall function by id....\n");  
    SysParms* parameters = get_syscall_parameters();
    switch(parameters->syscall_id) //syscall id is being stored in the ax register, moving the function pointer to the void pointer and later calling the function with parameters which are being passed through registers
    {
        case PRINTS:
            print_str(parameters->first);
            break;
        case PRINTC:
            print_char(parameters->first);
            break;
        case PRINTI:
            print_unsigned_int(parameters->first);
            break;
        case PRINTH:
            print_hex(parameters->first);
            break;
        case MALL:;
            uint32 *num = alloc(parameters->first); //mallocing in the size given in the register at userspace ring
            parameters->second = num; //return register
            break;
        case FRE:
            free(parameters->first);
            break;
        case MAKE_FILE:
            make_file(parameters->first,USERSPACE_RING);
            break;
        case REMOVE_FILE:
            remove_file(parameters->first);
            break;
        case READ_FILE:;
            char* ret = read_file(parameters->first);
            parameters->second = ret;
            break;
        case MAKE_DIR:
            make_directory(parameters->first);
            break;
        case DEL_DIR:
            /////TBA
            break;
        case CHANGE_DIR:
            change_directory(parameters->first);
            break;
        case BACK_CHANGE_DIR:
            remove_last_directory_from_path_str();
            break;
        case LIST_FILES:;
            char* retu = list_files();
            parameters->second = retu;
            break;
        case RENAME_FILE:
            break;
        case LSOF:;
            lsof();
            break;
        case SYSTEM_PATH:;
            char* ret3 = current_system_directory();
            parameters->second = ret3;
            break;
        case WRITE_FILE:
            write_file(parameters->first,parameters->second);
            break;
        case CLEAR_SCREEN:
            print_clear();
            break;
        default:
            print_str("Syscall by the id of ");
            print_unsigned_int(parameters->syscall_id);
            print_str(" does not exist!\n");
            break;
    }
}