#ifndef BRIDGE_H
#define BRIDGE_H

#include "../../Helpers/helper.h"
#include "../../Helpers/types.h"
#include "../../Descriptors/isr.h"

void syscall_prints(char* str);
void syscall_printc(char ch);
void syscall_printh(uint32 num);
void syscall_printi(uint32 num);
uint32* syscall_malloc(uint32 size);
void syscall_free(uint32 address);
void syscall_makeFile(char* name);
char* syscall_readFile(char* name);
void syscall_makeDir(char* name);
void syscall_removeDir();
void syscall_removeFile(char* name);
void syscall_changeDir(char* name);
void syscall_BackChangeDir();
char* syscall_listFiles();
void syscall_mvFile();
void syscall_writeFile(char* name,char* data);
void syscall_lsof();
char* syscall_systemPath();
void syscall_cls();

#endif