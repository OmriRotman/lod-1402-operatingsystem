#include "Bridge.h"

/*
macro to call prints syscall
input: str pointer to print
output: none
*/
void syscall_prints(char* str)
{
    set_syscall_parameters(PRINTS,str,0,0,0,0);
    asm volatile("int $0x13");
}

/*
macro to call printc syscall
input:char to print
output:none
*/
void syscall_printc(char ch)
{
    set_syscall_parameters(PRINTC,ch,0,0,0,0);
    asm volatile("int $0x13");
}

/*
syscall to print uint32 in a hex (base 16) view
input:uint32 to print
output:none
*/
void syscall_printh(uint32 num)
{
    set_syscall_parameters(PRINTH,num,0,0,0,0);
    asm volatile("int $0x13");
}

/*
syscall to print uint32 in a decimal view
input:uint32 to print
output:none
*/
void syscall_printi(uint32 num)
{
    set_syscall_parameters(PRINTI,num,0,0,0,0);
    asm volatile("int $0x13");
}

/*syscall that mallocs an array in a size given
input:uint32 size to allocate
output:pointer to the array's starting pointer*/
uint32* syscall_malloc(uint32 size)
{
    set_syscall_parameters(MALL,size,0,0,0,0);
    asm volatile("int $0x13");
    SysParms* parameters = get_syscall_parameters();
    return parameters->second;
}

/*
syscall that frees an allocated array in the address given
input:pointer address to array to free
output:none
*/
void syscall_free(uint32 address)
{
    set_syscall_parameters(FRE,address,0,0,0,0);
    asm volatile("int $0x13");
}

/*
syscall that makes a file in the name given
input:filename to create
output:none
*/
void syscall_makeFile(char* name)
{
    set_syscall_parameters(MAKE_FILE,name,0,0,0,0);
    asm volatile("int $0x13");
}

/*
syscall that reads a file in the filename given
input: filename to read
output:char array (being the content of the file)
*/
char* syscall_readFile(char* name)
{
    set_syscall_parameters(READ_FILE,name,0,0,0,0);
    asm volatile("int $0x13");
    SysParms* parameters = get_syscall_parameters();
    return parameters->second;
}

/*
syscall that creates a new directory in the name given
input:directory name
output:none
*/
void syscall_makeDir(char* name)
{
    set_syscall_parameters(MAKE_DIR,name,0,0,0,0);
    asm volatile("int $0x13");
}

/*
this syscall removes a directory in the name given
input:dirname to remove
output:none
*/
void syscall_removeDir()
{

}

/*
this function removes a file in the name given
input:filename to remove
output:none
*/
void syscall_removeFile(char* name)
{
    set_syscall_parameters(REMOVE_FILE,name,0,0,0,0);
    asm volatile("int $0x13");
}

/*
this syscall moves (cd) to the directory given
input:dirname to cd to
output:none
*/
void syscall_changeDir(char* name)
{
    set_syscall_parameters(CHANGE_DIR,name,0,0,0,0);
    asm volatile("int $0x13");
}

/*
this syscall cd to the last directory in the syspath
input:none
output:none
*/
void syscall_BackChangeDir()
{
    set_syscall_parameters(BACK_CHANGE_DIR,0,0,0,0,0);
    asm volatile("int $0x13");
}

/*this syscall lists files in the current directory
input:none
output:char array (string) of the list of the filenames*/
char* syscall_listFiles()
{
    set_syscall_parameters(LIST_FILES,0,0,0,0,0);
    asm volatile("int $0x13");
    SysParms* parameters = get_syscall_parameters();
    return parameters->second;
}

/*
this syscall renames a file
input: old,new filenames
output:none
*/
void syscall_mvFile()
{

}

/*
this syscall writes data to a file
input:filename to write to,data to write into the file
output:none
*/
void syscall_writeFile(char* name,char* data)
{
    set_syscall_parameters(WRITE_FILE,name,data,0,0,0);
    asm volatile("int $0x13");
    SysParms* parameters = get_syscall_parameters();
    return parameters->second;
}

/*
this syscall lists the files in the current directory
input: none
output: none
*/
void syscall_lsof()
{
    set_syscall_parameters(LSOF,0,0,0,0,0);
    asm volatile("int $0x13");
}

/*
this syscall returns current syspath
input:none
output:string (char array), represents system's current path
*/
char* syscall_systemPath()
{
    set_syscall_parameters(SYSTEM_PATH,0,0,0,0,0);
    asm volatile("int $0x13");
    SysParms* parameters = get_syscall_parameters();
    return parameters->second;
}

/*
this syscall clears the screen
input:none
output:none
*/
void syscall_cls()
{
    set_syscall_parameters(CLEAR_SCREEN,0,0,0,0,0);
    asm volatile("int $0x13");
}