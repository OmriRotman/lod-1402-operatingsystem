#include "assistant.h"

/*this function gets the opcode from a function for example (man color) will return "man"
input:full command to parse (char*)
output:pointer to beggining of the opcode string*/
void get_opcode(char* input,char* opcode)
{
    get_untill(input,opcode,' ');
}

/*this function gets the parameter list from a function for example (man color) will return "color"
input:full command to parse (char*)
output:pointer to beggining of the parameter list string*/
void get_parameter_list(char* input, char* parameters)
{
    get_from(input, parameters, ' ');
}

/*this function translates a color to its numerical value
input:color name (char*) - Case sensitive , capital letters, output pointer to put the function's output
output:none (returns output through pointer)*/
int translate_color(char* color)
{
    if (str_cmp(color, "BLACK", get_length("BLACK")) == 0)
    {
        return 0;
    }
    else if(str_cmp(color, "BLUE", get_length("BLUE")) == 0)
    {
        return 1;
    }
    else if(str_cmp(color, "GREEN", get_length("GREEN")) == 0)
    {
        return 2;
    }
    else if(str_cmp(color, "CYAN", get_length("CYAN")) == 0)
    {
        return 3;
    }
    else if(str_cmp(color, "RED", get_length("RED")) == 0)
    {
        return 4;
    }
    else if(str_cmp(color, "MAGENTA", get_length("MAGENTA")) == 0)
    {
        return 5;
    }
    else if(str_cmp(color, "BROWN", get_length("BROWN")) == 0)
    {
        return 6;
    }
    else if(str_cmp(color, "GREY", get_length("GREY")) == 0)
    {
        return 7;
    }
    else if(str_cmp(color, "DARK_GREY", get_length("DARK_GREY")) == 0)
    {
        return 8;
    }
    else if(str_cmp(color, "BRIGHT_BLUE", get_length("BRIGHT_BLUE")) == 0)
    {
        return 9;
    }
    else if(str_cmp(color, "BRIGHT_GREEN", get_length("BRIGHT_GREEN")) == 0)
    {
        return 0xa;
    }
    else if(str_cmp(color, "BRIGHT_CYAN", get_length("BRIGHT_CYAN")) == 0)
    {
        return 0xb;
    }
    else if(str_cmp(color, "BRIGHT_RED", get_length("BRIGHT_RED")) == 0)
    {
        return 0xc;
    }
    else if(str_cmp(color, "BRIGHT_MAGENTA", get_length("BRIGHT_MAGENTA")) == 0)
    {
        return 0xd;
    }
    else if(str_cmp(color, "YELLOW", get_length("YELLOW")) == 0)
    {
        return 0xe;
    }
    else if(str_cmp(color, "WHITE", get_length("WHITE")) == 0)
    {
        return 0xf;
    }
    else
    {
        syscall_printc('\n');
        syscall_prints(color);
        syscall_prints(" is not supported!\nPlease use 'man color' to learn more\n");
        return -1;
    }
}

/*this function handles color command
input:parameters (char*) for example: (BLACK,BLUE)
output:none*/
void handle_color_command(char *par)
{
    char bc[MAX_COLORNAME_LENGTH];
    get_untill(par, bc, ','); //parses first color

    turn_to_capital_letters(bc); //makes first color capital for check

    int b = translate_color(bc); //translates the first color to integer

    get_from(par,bc,','); //parses second color

    turn_to_capital_letters(bc); //turns the second color to capital

    int f = translate_color(bc); //translates the color to integer

    if(b == -1 || f == -1)
    {
        return;
    }

    print_set_color(f,b); //changes the screen color
}

/*this function handles the man protocol
input:opcode to review
output:none (prints information about the command)
NOTE: Once file system will be implemented, man pages will be written to files*/
void man(char* opcode)
{
    if (str_cmp(opcode, "man", get_length("man")) == 0) //man the man command
    {
        syscall_prints("Man command gives description about system programs and protocols\nNo flags\nParameters - <Command name>\nAttention! System rooted programs and protocols can be listed using 'help'\n");
    }
    else if (str_cmp(opcode, "color", get_length("color")) == 0) //man the color comman
    {
        syscall_prints("This program colors the screen with colors given in the parameter\nNo flags\nParameters - <Background>,<Foreground> (Strings)\nAvailable colors:\nBLACK,BLUE,GREEN,CYAN,RED,MAGENTA,BROWN,GREY,DARK_GREY,BRIGHT_BLUE,BRIGHT_GREEN,BRIGHT_CYAN,BRIGHT_RED,BRIGHT_MAGENTA,YELLOW,WHITE\n(Case Sensitive!)\n");
    }
    else if(str_cmp(opcode, "echo", get_length("echo")) == 0)
    {
        syscall_prints("This program prints a wanted string to the screen.\nFlags:\n -e , remove spaces,\nParameters - '<String to print>'\nSupported features:\n/n - a new line\n/b - a backspace\n/t - a tab\n/v - a vertical tab\n/r - remove everything before this character apears\n");
    }
    ///
    else if(str_cmp(opcode, "help", get_length("help")) == 0)
    {
        syscall_prints("This program prints every available command of the terminal.\nFlags: None\nParameters - None\n");
    }
    else if(str_cmp(opcode, "lsof", get_length("lsof")) == 0)
    {
        syscall_prints("This program prints every File and Directory, and gives additional information about each.\nFlags: None\nParameters - None\n");
    }
    else if(str_cmp(opcode, "mkdir", get_length("mkdir")) == 0)
    {
        syscall_prints("This program creates a directory with a given name.\nFlags: None\nParameters - '<Directory name>'\n");
    }
    else if(str_cmp(opcode, "cd", get_length("cd")) == 0)
    {
        syscall_prints("This program changes the directory to an existance directory inside the current system directory (Changing forward)...\nFlags: None\nParameters - '<Directory name to move to>'\n");
    }
    else if(str_cmp(opcode, "cd..", get_length("cd..")) == 0)
    {
        syscall_prints("This program moves to the latest system directoty (Moving backwards)..\nFlags: None\nParameters - None\n");
    }
    else if(str_cmp(opcode, "rmdir", get_length("rmdir")) == 0)
    {
        syscall_prints("This program removes a directory and all the files under it.\nFlags: None\nParameters - '<Directory name to remove>'\n");
    }
    else if(str_cmp(opcode, "touch", get_length("touch")) == 0)
    {
        syscall_prints("This program creates a new file in the current system directory.\nFlags: None\nParameters - '<File name>'\n");
    }
    else if(str_cmp(opcode, "ls", get_length("ls")) == 0)
    {
        syscall_prints("This program lists the files and directories in the current system directory.\nFlags: None\nParameters - None\n");
    }
    else if(str_cmp(opcode, "rm", get_length("rm")) == 0)
    {
        syscall_prints("This program deletes a file in the current system directory.\nFlags: None\nParameters - '<File name>'\n");
    }
    else if(str_cmp(opcode, "cat", get_length("cat")) == 0)
    {
        syscall_prints("This program reads and prints the content of a given file to the screen.\nFlags: None\nParameters - '<File name>'\n");
    }
    else if(str_cmp(opcode, "syspath", get_length("syspath")) == 0)
    {
        syscall_prints("This program prints the current system path.\nFlags: None\nParameters - None\n");
    }
    else if(str_cmp(opcode, "clear", get_length("clear")) == 0)
    {
        syscall_prints("This program clears the text from the screen.\nFlags: None\nParameters - None\n");
    }
    else if(str_cmp(opcode, "write", get_length("write")) == 0)
    {
        syscall_prints("This program writes data into a given text file.\nFlags: None\nParameters - '<File to write into>', '<Data to write>'\n");
    }
}

/*this function prints a string given to the screen, together with flags and \X's
input:echo opcode's parameters (char*)
output:none*/
void echo(char* par)
{
    //if there are no flags given
    char text[MAX_COMMAND_SIZE];
    char print_text[MAX_COMMAND_SIZE];

    get_from(par,text,'"'); //parsing the data inside the ""
    get_untill(text,print_text,'"');

    int len = get_length(print_text);
    for(int i = 0;i<len;i++)
    {
        if(print_text[i] == PRINT_PROTOCOL_ASCII) //code for some kind of a protocol like \n
        {
            switch(print_text[i + 1])
            {
                case 'n':
                    syscall_printc('\n');
                    i++;
                    break;
                case 't':
                    syscall_printc('    ');
                    i++;
                    break;
                case 'b':
                    deleteLastVGA();
                    i++;
                    break;
                case 'v':
                    syscall_prints("\n\n\n\n");
                    i++;
                    break;
                case 'r':
                    for(int j = 0;j<i;j++)
                    {
                        deleteLastVGA();
                    }
                    i++;
                    break;
                default:
                    break;
            }
        }
        else
        {
            syscall_printc(print_text[i]);
        }
    }
    syscall_printc('\n');
}