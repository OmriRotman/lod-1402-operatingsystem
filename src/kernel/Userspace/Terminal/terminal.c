#include "terminal.h"

/*the terminal entry function
input:none
output:none*/
void terminal_entry()
{
    syscall_prints("Entered terminal..\n");
    while(true)
    {
        print_str("/~ ");
        char command[256];
        gets(command,256);
       
        char opcode[MAX_OPCODE_LENGTH];
        get_opcode(command,opcode);
        
        if (str_cmp(opcode, "color", get_length("color")) == 0) //man the color comman
        {
            char par[MAX_COMMAND_SIZE] = "";
            get_parameter_list(command,par);
            
            handle_color_command(par);
        }
        else if (str_cmp(opcode, "man", get_length("man")) == 0) //man the man command
        {
            char par[MAX_COMMAND_SIZE];
            get_parameter_list(command,par);

            man(par);
        }
        else if (str_cmp(opcode, "echo", get_length("echo")) == 0) //man the color comman
        {   
            char par[MAX_COMMAND_SIZE];
            get_parameter_list(command,par);

            echo(par);
        }
        else if (str_cmp(opcode, "mkdir", get_length("mkdir")) == 0)
        {
            char* par = (char*)syscall_malloc(sizeof(char)*MAX_COMMAND_SIZE);
            get_parameter_list(command,par);
            syscall_makeDir(par);
        }
        //
        else if (str_cmp(opcode, "cd", get_length("cd")) == 0)
        {
            char par[MAX_COMMAND_SIZE];
            get_parameter_list(command,par);
            syscall_changeDir(par);
        }
        else if (str_cmp(opcode, "cd..", get_length("cd..")) == 0)
        {
            syscall_BackChangeDir();
        }
        else if (str_cmp(opcode, "rmdir", get_length("rmdir")) == 0)
        {

        }
        else if (str_cmp(opcode, "touch", get_length("touch")) == 0)
        {
            char* par = (char*)syscall_malloc(sizeof(char)*MAX_COMMAND_SIZE);
            get_parameter_list(command,par);
            syscall_makeFile(par);
        }
        else if (str_cmp(opcode, "ls", get_length("ls")) == 0)
        {
            char* files = syscall_listFiles();
            syscall_prints(files);
            syscall_printc('\n');
            syscall_free(files);
        }
        else if (str_cmp(opcode, "write", get_length("write")) == 0)
        {
            char* par = (char*)syscall_malloc(sizeof(char)*MAX_COMMAND_SIZE);
            get_parameter_list(command,par);
            char* name = (char*)syscall_malloc(sizeof(char)*MAX_COMMAND_SIZE);
            get_opcode(par,name);
            char* data = (char*)syscall_malloc(sizeof(char)*MAX_COMMAND_SIZE);
            char* new_data = (char*)syscall_malloc(sizeof(char)*MAX_COMMAND_SIZE);
            get_parameter_list(par,data);
            memcpy(new_data,data,get_length(data)); //moving the data to a new spot so it wont get deleted
            syscall_writeFile(name,new_data);
            syscall_free(name);
            syscall_free(par);
            syscall_free(data);
        }
        else if (str_cmp(opcode, "rm", get_length("rm")) == 0)
        {
            char* par = (char*)syscall_malloc(sizeof(char)*MAX_COMMAND_SIZE);
            get_parameter_list(command,par);
            syscall_removeFile(par);
            free(par);
        }
        else if (str_cmp(opcode, "cat", get_length("cat")) == 0)
        {
            char* par = (char*)syscall_malloc(sizeof(char)*MAX_COMMAND_SIZE);
            get_parameter_list(command,par);
            char* content = syscall_readFile(par);
            if(content != NULL)
            {
                syscall_prints(content);
                syscall_printc('\n');
            }
            syscall_free(par);
        }
        else if (str_cmp(opcode, "lsof", get_length("lsof")) == 0)
        {
            syscall_lsof();
        }
        else if (str_cmp(opcode, "syspath", get_length("syspath")) == 0)
        {
            char* content = syscall_systemPath();
            syscall_prints(content);
            syscall_printc('\n');
            free(content);
        }
        else if (str_cmp(opcode, "help", get_length("help")) == 0)
        {
            syscall_prints("Available commands: echo,color,man,help,lsof,mkdir,cd,cd..,rmdir,touch,ls,rm,cat,syspath,clear,write\nTo recieve information about each command, use man command\n");
        }
        else if (str_cmp(opcode, "clear", get_length("clear")) == 0)
        {
            syscall_cls();
        }
        else if (str_cmp(opcode, "exit", get_length("exit")) == 0)
        {
            return;
        }
        else
        {
            syscall_prints(opcode);
            syscall_prints(" is not recognized as an internal or external command or an operable program!\n");
        }   
    }
}