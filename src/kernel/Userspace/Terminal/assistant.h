#ifndef ASSISTANT_H
#define ASSISTANT_H

#include "../../drivers/keyboard/keyboard_driver.h"
#include "../../drivers/screen/screen_driver.h"
#include "../../Helpers/helper.h"
#include "../../Helpers/types.h"
#include "../../Memory/heap_allocation.h"
#include "../../FileSystem/FileSystem.h"
#include "../../Helpers/Vectors/String/String.h"
#include "../../Helpers/Vectors/uint32/VectorInt.h"
#include "../Bridge/Bridge.h"

#define MAX_OPCODE_LENGTH 10
#define MAX_COLORNAME_LENGTH 20
#define MAX_COMMAND_SIZE 80
#define PRINT_PROTOCOL_ASCII 92

void get_opcode(char* input,char* opcode);
void get_parameter_list(char* input, char* parameters);
int translate_color(char *color);
void man(char* opcode);
void handle_color_command(char *par);
void echo(char* par);

#endif