#ifndef KEYBOARD_DRIVER_H
#define KEYBOARD_DRIVER_H

typedef unsigned char uint8;
typedef char sint8;
typedef unsigned short uint16;
typedef short sint16;
typedef unsigned int uint32;
typedef int sint32;

#define bool int
#define true 1
#define false 0
#define NULL 0

#define INPUT_PORT_ADDRESS 0x60
#define BACKSPACE_ASCII 8
#define LEFT_ARROW 14
#define RIGHT_ARROW 15

static bool CAPSflag = false;
static bool SHIFTflag = false;

static int XLOC = 0;
static int YLOC = 0;
static int xs = 80;
static int ys = 25;

uint16* vga_buffer;

void gets(char *to_ret,int size);
void print_string_with_cursor(char* val, int cursor_pos);
void remove_in_middle(char* val, int pos, int size, int capacity);
void move_backwards(char* val, int size);
void insert_in_middle(char* val,char curr,int pos,int size,int capacity);
void move_forward(char* val,int size);
char getchar();
static inline void io_wait(void);
char getch();
inline uint16 getScanCode16();

#endif