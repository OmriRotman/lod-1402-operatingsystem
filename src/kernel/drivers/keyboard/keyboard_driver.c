#include "keyboard_driver.h"


/*
this function gets scancode from input port 16 bits
input:none
output:scancode from input port
*/
uint16 getScanCode16()
{
  uint16 scancode = inportb16(INPUT_PORT_ADDRESS);
  return scancode;
}

/*
this function reads the input port and returns the ascii value of the scancode read
input:none
output:char from input port
*/
char getch()
{
  outb(INPUT_PORT_ADDRESS,0x00);  //clean input port so it gets it once and wont re-read the same key hit
  uint16 input = getScanCode16();   //reads port and gets scancode. 
  ///////
  //From this point over I will convert the I/O port scancode to ascii value and return it unless its a special key press like CAPS or SHIFT and etc..
  //////
  /////////////////////////////////////////NUMERIC VALUES////////////////////////////
  if(input == 2 )//|| input == 0x82)
  {
    if(SHIFTflag == true)
    {
      return '!';
    }
    return '1';
  }
  if(input == 3 )//|| input == 0x83)
  {
    if(SHIFTflag == true)
    {
      return '@';
    }
    return '2';
  }
  if(input == 4 )//|| input == 0x84)
  {
    if(SHIFTflag == true)
    {
      return '#';
    }
    return '3';
  }
  if(input == 5 )//|| input == 0x85)
  {
    if(SHIFTflag == true)
    {
      return '$';
    }
    return '4';
  }
  if(input == 6 )//|| input == 0x86)
  {
    if(SHIFTflag == true)
    {
      return '%';
    }
    return '5';
  }
  if(input == 7 )//|| input == 0x87)
  {
    if(SHIFTflag == true)
    {
      return '^';
    }
    return '6';
  }
  if(input == 8 )//|| input == 0x88)
  {
    if(SHIFTflag == true)
    {
      return '&';
    }
    return '7';
  }
  if(input == 9 )//|| input == 0x89)
  {
    if(SHIFTflag == true)
    {
      return '*';
    }
    return '8';
  }
  if(input == 0x0A )//|| input == 0x8A)
  {
    if(SHIFTflag == true)
    {
      return '(';
    }
    return '9';
  }
  if(input == 0x0B )//|| input == 0x8B)
  {
    if(SHIFTflag == true)
    {
      return ')';
    }
    return '0';
  }
  ////////////////////////////////////////////Letter values//////////////////////////
  if(input == 0x1E )//|| input == 0x9E)
  {
    if(CAPSflag == true)
    {
      return 'A';
    }
    return 'a';
  }
  if(input == 0x30 )//|| input == 0xB0)
  {
    if(CAPSflag == true)
    {
      return 'B';
    }
    return 'b';
  }
  if(input == 0x2E )//|| input == 0xAE)
  {
    if(CAPSflag == true)
    {
      return 'C';
    }
    return 'c';
  }
  if(input == 0x20 )//|| input == 0xA0)
  {
    if(CAPSflag == true)
    {
      return 'D';
    }
    return 'd';
  }
  if(input == 0x12 )//|| input == 0x92)
  {
    if(CAPSflag == true)
    {
      return 'E';
    }
    return 'e';
  }
  if(input == 0x21 )//|| input == 0xA1)
  {
    if(CAPSflag == true)
    {
      return 'F';
    }
    return 'f';
  }
  if(input == 0x22 )//|| input == 0xA2)
  {
    if(CAPSflag == true)
    {
      return 'G';
    }
    return 'g';
  }
  if(input == 0x23 )//|| input == 0xA3)
  {
    if(CAPSflag == true)
    {
      return 'H';
    }
    return 'h';
  }
  if(input == 0x17 )//|| input == 0x97)
  {
    if(CAPSflag == true)
    {
      return 'I';
    }
    return 'i';
  }
  if(input == 0x24 )//|| input == 0xA4)
  {
    if(CAPSflag == true)
    {
      return 'J';
    }
    return 'j';
  }
  if(input == 0x25 )//|| input == 0xA5)
  {
    if(CAPSflag == true)
    {
      return 'K';
    }
    return 'k';
  }
  if(input == 0x26 )//|| input == 0xA6)
  {
    if(CAPSflag == true)
    {
      return 'L';
    }
    return 'l';
  }
  if(input == 0x32 )//|| input == 0xB2)
  {
    if(CAPSflag == true)
    {
      return 'M';
    }
    return 'm';
  }
  if(input == 0x31 )//|| input == 0xB1)
  {
    if(CAPSflag == true)
    {
      return 'N';
    }
    return 'n';
  }
  if(input == 0x18 )//|| input == 0x98)
  {
    if(CAPSflag == true)
    {
      return 'O';
    }
    return 'o';
  }
  if(input == 0x19 )//|| input == 0x99)
  {
    if(CAPSflag == true)
    {
      return 'P';
    }
    return 'p';
  }
  if(input == 0x10 )//|| input == 0x90)
  {
    if(CAPSflag == true)
    {
      return 'Q';
    }
    return 'q';
  }
  if(input == 0x13 )//|| input == 0x93)
  {
    if(CAPSflag == true)
    {
      return 'R';
    }
    return 'r';
  }
  if(input == 0x1F )//|| input == 0x9F)
  {
    if(CAPSflag == true)
    {
      return 'S';
    }
    return 's';
  }
  if(input == 0x14 )//|| input == 0x94)
  {
    if(CAPSflag == true)
    {
      return 'T';
    }
    return 't';
  }
  if(input == 0x16 )//|| input == 0x96)
  {
    if(CAPSflag == true)
    {
      return 'U';
    }
    return 'u';
  }
  if(input == 0x2F )//|| input == 0xAF)
  {
    if(CAPSflag == true)
    {
      return 'V';
    }
    return 'v';
  }
  if(input == 0x11 )//|| input == 0x91)
  {
    if(CAPSflag == true)
    {
      return 'W';
    }
    return 'w';
  }
  if(input == 0x2D )//|| input == 0xAD)
  {
    if(CAPSflag == true)
    {
      return 'X';
    }
    return 'x';
  }
  if(input == 0x15 )//|| input == 0x95)
  {
    if(CAPSflag == true)
    {
      return 'Y';
    }
    return 'y';
  }
  if(input == 0x2C )//|| input == 0xAC)
  {
    if(CAPSflag == true)
    {
      return 'Z';
    }
    return 'z';
  }
  ////////////////////////////////////////////////////SPECIAL KEYS///////////////////////////
  if(input == 0x3A )//|| input == 0xBA) //CAPS LOCK
  {
    if(CAPSflag == true) //SWITCHING CAPS FLAG
    {
      CAPSflag = false;
    }
    else
    {
      CAPSflag = true;
    }
  }
  if(input == 0x36 )//|| input == 0xB6) //SHIFT LOCK (RIGHT)
  {
     if(SHIFTflag == true) //SWITCHING SHIFT FLAG
    {
      SHIFTflag = false;
    }
    else
    {
      SHIFTflag = true;
    }
  }
  if(input == 0x2A )//|| input == 0xAA) //SHIFT LOCK (LEFT)
  {
     if(SHIFTflag == true) //SWITCHING SHIFT FLAG
    {
      SHIFTflag = false;
    }
    else
    {
      SHIFTflag = true;
    }
  }
  if(input == 0x39 )//|| input == 0xB9) //SPACE
  {
    return ' ';
  }
  if(input == 0x0F )//|| input == 0x8F) //TAB
  {
    return 9;
  }
  if(input == 0x1C) //ENTER \N
  {
    return '\n';
  } 
  if(input == 0x0E) // Backspace
  {
    return BACKSPACE_ASCII;
  } 
  if(input == 0x3F) // left (F5)
  {
    return LEFT_ARROW;
  } 
   if(input == 0x40) // right (F6)
  {
    return RIGHT_ARROW;
  } 
  /////////////////////////////////SIGNS//////////////////////////////
  if(input == 0x0C)
  {
    if(SHIFTflag == true)
    {
      return '_';
    }
    return '-';
  } 
  if(input == 0x0D)
  {
    if(SHIFTflag == true)
    {
      return '+';
    }
    return '=';
  } 
  if(input == 0x33)
  {
    if(SHIFTflag == true)
    {
      return '<';
    }
    return ',';
  } 
  if(input == 0x34)
  {
    if(SHIFTflag == true)
    {
      return '>';
    }
    return '.';
  } 
  if(input == 0x35)
  {
    if(SHIFTflag == true)
    {
      return '?';
    }
    return '/';
  } 
  if(input == 0x2B)
  {
    if(SHIFTflag == true)
    {
      return '|';
    }
    return 92;
  } 
  if(input == 0x28)
  {
    if(SHIFTflag == true)
    {
      return '"';
    }
    return 39;
  } 
  if(input == 0x27)
  {
    if(SHIFTflag == true)
    {
      return ':';
    }
    return ';';
  } 
  if(input == 0x1A)
  {
    if(SHIFTflag == true)
    {
      return '{';
    }
    return '[';
  } 
  if(input == 0x1B)
  {
    if(SHIFTflag == true)
    {
      return '}';
    }
    return ']';
  } 
  return 0; //returning NULL if neither of the above is correct
  ////////////////////////END/////////////////////
}

/*
this function is a short stopper for the i/o ports
input: none
output:none
*/
static inline void io_wait(void)
{
    outb(0x80, 0);
}

/*
this function gets a char and waits for it to be entered
input:none
output:char given by the user
*/
char getchar()
{
  char d = getch();
  while(d == 0)
  {
    d = getch();
  }
  return d;
}
////////////////////////////////////////////////////////////////////////////

/*this function moves every value one step forward in a string
input:string,size
output:none*/
void move_forward(char* val,int size)
{
	for (int i = size - 1; i >= 0; i--)
	{
		val[i + 1] = val[i];
	}
}

/*this function inserts a char in middle of a string
input:string,character to insert,position to insert in,size of string,capacity of string
output:none*/
void insert_in_middle(char* val,char curr,int pos,int size,int capacity)
{
	if (size == capacity)
	{
		return;
	}
	
	char* val2 = val + pos*sizeof(char);
	move_forward(val2,size-pos);
	
	val[pos] = curr;
}

/*this function moves every value one step backwards in a string
input:string,size
output:none*/
void move_backwards(char* val, int size)
{
	for (int i = 0; i < size; i++)
	{
		val[i] = val[i + 1];
	}
}

/*this function removes a char in middle of a string
input:string,character to insert,position to insert in,size of string,capacity of string
output:none*/
void remove_in_middle(char* val, int pos, int size, int capacity)
{
	if (size == 0)
	{
		return;
	}

	char* val2 = val + pos * sizeof(char);
	move_backwards(val2, size - pos);

	val[size] = 0;
}

/*this function prints a string with a cursor
input:string to print,cursor loc
output:none*/
void print_string_with_cursor(char* val, int cursor_pos)
{
	int i = 0;
	do
	{
		if (i - 1== cursor_pos)
		{
			print_char('|');
		}
		print_char(val[i]);
		i++;
	} while (val[i] != 0);
}

/*
this function gets a string input till enter is hit
input: string to get (char*),size of the buffer (int)
output: none (returns none just changes pointer value)
*/
void gets(char *to_ret,int size)
{
  for(int i = 0;i<size;i++)
  {
    to_ret[i] = 0;
  }

  int arrow_detector = 0;
  int length = 0;
  int can_del = 0;
  int i = 0;
  while(true) //goes till enter
  {
    if(i > size - 2) //prevents buffer overflow
    {
      return;
    }
    if(i < 0)
    {
      i = 0;
    }
    char curr = getchar();
    if(curr == BACKSPACE_ASCII)  //backspace handle
    {
      if(arrow_detector == 0)
      {
        if(can_del > 0)
        {
          length--;
          i -= 1;
          to_ret[i + 1] = '\0';
          deleteLastVGA();
          can_del--;
        }
      }
      else
      {
         for(int i = 0;i<length;i++)
          {
            deleteLastVGA();
          }

          remove_in_middle(to_ret,length-arrow_detector - 1,length,size);
          i--;
          length--;
          print_str(to_ret);
          for(int i = 0; i< arrow_detector;i++)
          {
            cursor_back();
          }
      }
    }
    else if(curr == LEFT_ARROW)
    {
      if(arrow_detector < length)
      {
        cursor_back();
        arrow_detector++;
      }
    }
    else if(curr == RIGHT_ARROW)
    {
      if(arrow_detector > 0)
      {
        cursor_forward();
        arrow_detector--;
      }
    }
    else if(curr != '\n') //gets a normal char
    {
      if(i>=0)
      {
        if(arrow_detector == 0)
        {
          to_ret[i] = curr;
          print_char(to_ret[i]);
          length++;
          i++;
          can_del++;
        }
        else
        {
          for(int i = 0;i<length;i++)
          {
            deleteLastVGA();
          }

          insert_in_middle(to_ret,curr,length-arrow_detector,length,size);
          i++;
          length++;
          print_str(to_ret);
          for(int i = 0; i< arrow_detector;i++)
          {
            cursor_back();
          }
        }
      }
    }
    else //finishes
    {
      if(i != 0)
      {
        to_ret[i] = '\0';
        print_char('\n');
        return;
      }
    }
  }
}
