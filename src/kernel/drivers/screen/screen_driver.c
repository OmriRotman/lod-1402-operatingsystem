#include "screen_driver.h"


static bool CAPSflag = false;
static bool SHIFTflag = false;

static uint8 background_color = BLACK;
static uint8 text = WHITE;
static int XLOC = 0;
static int YLOC = 0;

static CURSOR_X = 0;
static CURSOR_Y = 0;

static int xs = 80;
static int ys = 25;
/*
16 bit video buffer elements(register ax)
8 bits(ah) higher : 
  lower 4 bits - forec olor
  higher 4 bits - back color

8 bits(al) lower :
  8 bits : ASCII character to print

this function creates a memory cell value for character and colors
input: character (8bit),text color (8bit),background (8bit)
output: (16bit) memory cell value*/
static uint16 vga_entry(unsigned char ch, uint8 fore_color, uint8 back_color) 
{
  uint16 ax = 0;
  uint8 ah = 0, al = 0;

  ah = back_color;
  ah <<= 4;
  ah |= fore_color;
  ax = ah;
  ax <<= 8;
  al = ch;
  ax |= al;

  return ax;
}

//clear video buffer array
/*this function clears the screen or colors it
input:buffer and colors (8bit)
output:buffer cleansed*/
static void clear_vga_buffer(uint16 **buffer, uint8 fore_color, uint8 back_color)
{
  uint32 i;
  for(i = 0; i < BUFSIZE; i++){
    (*buffer)[i] = vga_entry(NULL, fore_color, back_color);
  }
}

//initialize vga buffer
/*this function initializes the video memory array so I can write to the screen
input: text color (8bit), background color (8bit)
output:initialized array*/
static void init_vga(uint8 fore_color, uint8 back_color)
{
  vga_buffer = (uint16*)VGA_ADDRESS;  //point vga_buffer pointer to VGA_ADDRESS 
  clear_vga_buffer(&vga_buffer, fore_color, back_color);  //clear buffer
}

/*this function gets a row number and cleans it for its color. rows starting from 0.*/
void clear_row(int row)
{
  //the first index of the line given
  int starting = row*xs;
  for(int i=starting;i <= xs;i++)
  {
    //clears the line
    vga_buffer[i] = vga_entry(' ',text,background_color);
  }
}


/*this function goes down to a newline on the screen
input:none
output:new line created*/
void print_newline() 
{
	    XLOC = 0; //start from the start of line
	
	    if (YLOC < ys - 1) //if theres room for a new line go down and exit
      {
	        YLOC++;
	        return;
	    }
      //if theres no room for a new line move everything up and clear last row
	    for (int row = 1; row < ys; row++) 
      {
	        for (int col = 0; col < xs; col++) 
          {
	            uint16 character = vga_buffer[col + xs * row];
	            vga_buffer[col + xs * (row - 1)] = character;
              vga_buffer[col + xs * row] = vga_entry(' ',text,background_color);
	        }
	    }
      clear_row(25);
	}

/*this function clears the screen and starts the video memory array
input:none
output:none*/
void print_clear()
{
    //clear screen
    init_vga(text, background_color);
}

/*this function prints a character to the screen
input:character to print
output:printed character*/
void print_char(char character)
{
  if(character == 0) //DONT PRINT NULL
  {
    return;
  }

  if(character == 127)
  {
    return;
  }
	
  if (character == '\n') 
  {
	  print_newline(); //if thats the char given make a newline. return so \n wont get written
	  return;
	}
	if (XLOC >= xs) 
  {
	  print_newline(); //if the room in the line (X) is over go to a newline
	}

  uint16 to_print = vga_entry(character,text,background_color); //printing character
  vga_buffer[XLOC + xs*YLOC] = to_print;
  move_cursor(XLOC,YLOC);
	XLOC++;
}

/*this function deletes last character on the screen
input:none
output:none*/
void deleteLastVGA()
{
  XLOC--;
  uint16 to_print = vga_entry(' ',text,background_color); //printing character
  vga_buffer[XLOC + xs*YLOC] = to_print;
  move_cursor(XLOC,YLOC);
}

/*this function priunts a list pof chars to eventually print a string
input:string to print (char*)
output:printed string*/
void print_str(char* string)
{
  //Just running over the array untill finds char 0 and print all.
     for (int i = 0;i>=0; i++) 
     {
	        char character = (uint8) string[i];
	
	        if(character == '\0') 
          {
	            return;
	        }
          
          print_char(character);
     }
}

/*this function gets foreground and background and sets screen color to it without changing the characters*/
void print_set_color(uint8 foreground, uint8 background)
{
  //like clear screen just changing color.
    text = foreground;
    background_color = background;
    uint32 i;
    for(i = 0; i < BUFSIZE; i++)
    {
        char character = *(vga_buffer + i) & 0xFF; //gets lower 8bits of char
        *(vga_buffer + i) = vga_entry(character, foreground, background);
    }
}


void print_unsigned_int(uint32 n)
{
    if (n == 0)
    {
        print_char('0');
        return;
    }

    int acc = n;
    char c[32];
    int i = 0;
    while (acc > 0)
    {
        c[i] = '0' + acc%10;
        acc /= 10;
        i++;
    }
    c[i] = 0;

    char c2[32];
    c2[i--] = 0;
    int j = 0;
    while(i >= 0)
    {
        c2[i--] = c[j++];
    }
    print_str(c2);
}

void print_hex(uint32 n)
{
    sint32 tmp;

    print_str("0x");

    char noZeroes = 1;

    int i;
    for (i = 28; i > 0; i -= 4)
    {
        tmp = (n >> i) & 0xF;
        if (tmp == 0 && noZeroes != 0)
        {
            continue;
        }
    
        if (tmp >= 0xA)
        {
            noZeroes = 0;
            print_char(tmp-0xA+'a' );
        }
        else
        {
            noZeroes = 0;
            print_char(tmp+'0' );
        }
    }
  
    tmp = n & 0xF;
    if (tmp >= 0xA)
    {
        print_char(tmp-0xA+'a');
    }
    else
    {
        print_char(tmp+'0');
    }

}

/*this function uses I/O ports to print the cursor to the screen
input:x,y coordinates (uint16)
output:none*/
void move_cursor(uint16 x,uint16 y)
{
  CURSOR_X = x;
  CURSOR_Y = y;
   // The screen is 80 characters wide...
  uint16 cursorLocation = y * 80 + x; //get exact point by multiplying line size in y and adding x to the loc in the line
  outb(0x3D4, 14);                  // Tell the VGA board we are setting the high cursor byte.
  outb(0x3D5, cursorLocation >> 8); // Send the high cursor byte.
  outb(0x3D4, 15);                  // Tell the VGA board we are setting the low cursor byte.
  outb(0x3D5, cursorLocation);      // Send the low cursor byte.
}

void cursor_back()
{
  CURSOR_X --;
  move_cursor(CURSOR_X,CURSOR_Y);
}

void cursor_forward()
{
  CURSOR_X ++;
  move_cursor(CURSOR_X,CURSOR_Y);
}
/*
this function writes to port given
input: port,value to write (both uint16 WORD value)
output:none
*/
void outb(uint16 port, uint16 val)
{
    asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
    /* There's an outb %al, $imm8  encoding, for compile-time constant port numbers that fit in 8b.  (N constraint).
     * Wider immediate constants would be truncated at assemble-time (e.g. "i" constraint).
     * The  outb  %al, %dx  encoding is the only option for all other cases.
     * %1 expands to %dx because  port  is a uint16_t.  %w1 could be used if we had the port number a wider C type */
}

void outb8(uint16 port, uint8 value)
{
    asm volatile ("outb %1, %0" : : "dN" (port), "a" (value));
}


/*
this function reads 8bit value from port given
input:port address
output:8bit value read
*/
uint8 inportb8(uint16 port)
{
    uint8 ret;
    asm volatile ("inb %1, %0" : "=a" (ret) : "dN" (port));
    return ret;
}

/*
this function reads 16bit value from port given
input:port address
output:16bit value read
*/
uint16 inportb16(uint16 port)
{
    uint16 ret;
    asm volatile ("inb %1, %0" : "=a" (ret) : "dN" (port));
    return ret;
}