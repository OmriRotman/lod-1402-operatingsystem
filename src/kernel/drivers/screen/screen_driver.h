#ifndef SCREEN_DRIVER_H
#define SCREEN_DRIVER_H


typedef unsigned char uint8;
typedef char sint8;
typedef unsigned short uint16;
typedef short sint16;
typedef unsigned int uint32;
typedef int sint32;

#define bool int
#define true 1
#define false 0
#define NULL 0

#define INPUT_PORT_ADDRESS 0x60
#define BACKSPACE_ASCII 8
#define LEFT_ARROW 14
#define RIGHT_ARROW 15


#define XLEN 25 //ammount of text cells in a line
#define YLEN 80 //ammount of lines
#define VGA_ADDRESS 0xB8000 //first index of video memory segment
#define BUFSIZE 2200 // screen size


uint16* vga_buffer;


//this enum is the color enum
enum vga_color {
    BLACK,
    BLUE,
    GREEN,
    CYAN,
    RED,
    MAGENTA,
    BROWN,
    GREY,
    DARK_GREY,
    BRIGHT_BLUE,
    BRIGHT_GREEN,
    BRIGHT_CYAN,
    BRIGHT_RED,
    BRIGHT_MAGENTA,
    YELLOW,
    WHITE,
};
static uint16 vga_entry(unsigned char ch, uint8 fore_color, uint8 back_color);
static void clear_vga_buffer(uint16 **buffer, uint8 fore_color, uint8 back_color);
static void init_vga(uint8 fore_color, uint8 back_color);
void clear_row(int row);
void print_newline();
void print_clear();
void print_char(char character);
void deleteLastVGA();
void print_str(char* string);
void print_set_color(uint8 foreground, uint8 background);
void print_unsigned_int(uint32 n);
void print_hex(uint32 n);
void move_cursor(uint16 x,uint16 y);
uint16 inportb16(uint16 port);
uint8 getScanCode8();
uint8 inportb8(uint16 port);
void outb(uint16 port, uint16 val);
void outb8(uint16 port, uint8 value);

void cursor_forward();
void cursor_back();
#endif