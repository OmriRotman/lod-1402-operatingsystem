#ifndef TYPES_H
#define TYPES_H

typedef unsigned char uint8;
typedef char sint8;
typedef unsigned short uint16;
typedef short sint16;
typedef unsigned int uint32;
typedef int sint32;
typedef long long sint64;
typedef unsigned long long uint64;

#define bool uint8
#define true 1
#define false 0
#define NULL 0

#define KERNELL_RING 0 
#define USERSPACE_RING 3

//defining id's for syscalls (ax holds this identifier)
enum SYSCALLS{PRINTS = 1, PRINTC, PRINTI,PRINTH,MALL,FRE,MAKE_FILE,REMOVE_FILE,READ_FILE,MAKE_DIR,DEL_DIR,CHANGE_DIR,BACK_CHANGE_DIR,LIST_FILES,RENAME_FILE,LSOF,SYSTEM_PATH,WRITE_FILE,CLEAR_SCREEN};


#endif