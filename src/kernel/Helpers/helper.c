#include "helper.h"
// Copy len bytes from src to dest.
void memcpy(uint8 *dest, const uint8 *src, uint32 len)
{
    const uint8 *sp = (const uint8 *)src;
    uint8 *dp = (uint8 *)dest;
    for(; len != 0; len--) *dp++ = *sp++;
}

void memcpy_uint32(uint32 *dest, uint32 *src, uint32 len)
{
    for(int i = 0;i<len;i++)
    {
        dest[i] = src[i];
    }
}

// Write len copies of val into dest.
void memset(uint8 *dest, uint8 val, uint32 len)
{
    uint8 *temp = (uint8 *)dest;
    for ( ; len != 0; len--) *temp++ = val;
}

// Compare two strings. Should return -1 if 
// str1 < str2, 0 if they are equal or 1 otherwise.
int strcmp(char *str1, char *str2)
{
      int i = 0;
      int failed = 0;
      while(str1[i] != '\0' && str2[i] != '\0')
      {
          if(str1[i] != str2[i])
          {
              failed = 1;
              break;
          }
          i++;
      }
      // why did the loop exit?
      if( (str1[i] == '\0' && str2[i] != '\0') || (str1[i] != '\0' && str2[i] == '\0') )
          failed = 1;
  
      return failed;
}

// Copy the NULL-terminated string src into dest, and
// return dest.
char *strcpy(char *dest, const char *src)
{
    do
    {
      *dest++ = *src++;
    }
    while (*src != 0);
}

// Concatenate the NULL-terminated string src onto
// the end of dest, and return dest.
char *strcat(char *dest, const char *src)
{
    while (*dest != 0)
    {
        *dest = *dest++;
    }

    do
    {
        *dest++ = *src++;
    }
    while (*src != 0);
    return dest;
}

extern void panic(const char *message, const char *file, uint32 line)
{
    // We encountered a massive problem and have to stop.
    asm volatile("cli"); // Disable interrupts.

    print_str("PANIC(");
    print_str(message);
    print_str(") at ");
    print_str(file);
    print_str(":");
    print_unsigned_int(line);
    print_str("\n");
    // Halt by going into an infinite loop.
    for(;;);
}

extern void panic_assert(const char *file, uint32 line, const char *desc)
{
    // An assertion failed, and we have to panic.
    asm volatile("cli"); // Disable interrupts.

    print_str("ASSERTION-FAILED(");
    print_str(desc);
    print_str(") at ");
    print_str(file);
    print_str(":");
    print_unsigned_int(line);
    print_str("\n");
    // Halt by going into an infinite loop.
    for(;;);
}

/*this function deletes the \n in the array (from the input)
input:char* to delete from and size (int)
output:none (changes values in addresses)*/
void fix_string(char* n, int size)
{
    for (int i = 0; i < size; i++)
    {
        if (n[i] == '\n')
        {
            n[i] = 0;
        }
    }
}

/*this function returns a length of a string
input:string (char*)
output:length (int)*/
int get_length(char* a)
{
    int i = 0;
    while (a[i] != NULL)
    {
        i++;
    }
    return i;
}


/*
* this function checks if two strings are eqal in given length for both
* input: strings to compare (a,b [char*])
* output: 0 - true 1 - false
*/
int str_cmp(char* a, char* b, int len)
{
    if(get_length(a) != get_length(b))
    {
        return 1;
    }

    for (int i = 0; i < len; i++)
    {
        if (a[i] != b[i])
        {
            return 1;
        }
    }
    return 0;
}

/*this function splits the string with the char given and returns the first chunk (returns untill char apears)
input:input,output (char*),char to cut with
output:none*/
void get_untill(char* input, char* output,char a)
{
    int end = 0;
    for (int i = 0; i < get_length(input); i++) //goes over length of string and returns when gets a space
    {
        if (input[i] == a) //space detected - return
        {
            output[end] = 0; //define an end to the string
            return;
        }
        else //space not detected keep adding
        {
            output[i] = input[i];
        }
        end++;
    }
    output[end] = 0; //define an end to the string
}

/*this function splits the string with the char given and returns the last (second) chunk (returns everything after the char apears)
input:input,output (char*),char to cut with
output:none*/
void get_from(char* input, char* output,char a)
{
    int loc = 0;
    bool flag = false; //found a space?
    for (int i = 0; i < get_length(input); i++) //goes over length of string and returns when gets a space
    {
        if (flag) //space was detected add
        {
            output[loc++] = input[i]; //adding current one to the string and later adding +1 to the loc
        }

        if (input[i] == a) //space detected - found first space, start adding to string (parameter list is after space)
        {
            flag = true;
        }
    }
    output[loc] = 0; //define an end to the string
}

/*this function checks if a character is alphabetic and not capital
input:char to check
output:bool, (if it is not capital letter)*/
bool is_small(char a)
{
    return (a >= 'a' && a <= 'z');
}

/*this function turns a string to capital letters
input:char* string to convert
output:none*/
void turn_to_capital_letters(char* str)
{
    int length = get_length(str);
    for (int i = 0; i < length; i++)
    {
        if(is_small(str[i]) == true)
        {
            str[i] -= 32; //if the char is small and alphabethic then sub 32 to make it capital
        }
    }
}
///

/*checks if a number is between a certain range
input:start and ending point, number to check (int)
output:true/false if the num is in between the range (bool)*/
bool is_between(int start, int finish, int bet)
{
    for (int i = start; i < finish; i++)
    {
        if (i == bet)
        {
            return true;
        }
    }
    return false;
}

/*this function gets a length of a int (ammount of digits)
input:number (int)
output:length (int)*/
int get_int_len(int num)
{
    int count = 0;
    int pow = 1;
    while (num / pow > 0)
    {
        count++;
        pow *= 10;
    }
    return count;
}

/*this function gets a number and adds multiple 0 behind it in the pow given (power by 10)
input:number to return,pow ammount (int)
output:processed value (int)*/
int pow_10(int num, int p)
{
    for (int i = 0; i < p; i++)
    {
        num *= 10;
    }
    return num;
}

/*this function turns an int to a string
input:int value,string to return (char*)
output:none (just changes pointer's value)*/
void int_to_string(int value, char* result)
{
    int len = get_int_len(value);
    int p = pow_10(1, len - 1);
    for (int i = 0; i < len; i++)
    {
        result[i] = (char)(value / p + '0');
        value = value % p;
        p = p / 10;
    }
    for (int i = len; i < get_length(result); i++)
    {
        result[i] = NULL;
    }
}


/*this function makes a less than 4 byte long string which indicates a number to a 4 byte long
input:string to convert (char*)
output:none (changes memory cells)*/
void fix_string_to_range_4(char* tofix)
{
    while (get_length(tofix) < 4)
    {
        tofix[3] = tofix[2];
        tofix[2] = tofix[1];
        tofix[1] = tofix[0];
        tofix[0] = '0';
    }
}

/*this function gets a numeric value and turns it into a string
input: int to conver, string - output (char*)
output:none (changes memory cells)*/
void get_4_byte_string_from_int(int num, char* result)
{
    int_to_string(num, result);
    fix_string_to_range_4(result);
}

/*this function converts a string to an int
input:string to convert (char*)
output:int - string's numeric value*/
int atoi(char* str)
{
    int res = 0;
    for (int i = 0; str[i] != '\0'; ++i)
    {
        res = res * 10 + str[i] - '0';
    }
    return res;
}

/*this function converts a string to an int with the size given
input:string to convert (char*)
output:int - string's numeric value*/
int atoi_size(char* str, int size)
{
    int res = 0;
    for (int i = 0; i < size; ++i)
    {
        res = res * 10 + str[i] - '0';
    }
    return res;
}

/*this function writes to a buffer
input:Buffer to write to , starting point, value to write and size
output:none*/
void memory_write(char* BUF, int starting_point, char* data, int size)
{
    for (int i = 0; i < size; i++)
    {
        if (data[i] >= 41 && data[i] <= 176)
        {
            BUF[starting_point] = data[i];
            starting_point++;
        }
    }
}

/*this function reads to a buffer
input:Buffer to read from , starting point,size
output:none (changes memory values)*/
void memory_read(char* BUF, int starting_point, int length, char* output)
{
    int dat = 0;
    for (int i = starting_point; i < starting_point + length; i++)
    {
        output[dat] = BUF[i];
        dat++;
    }
    output[dat] = 0;
}

void lower_case(char* str)
{
    char* s = str;
    while(*s) 
    {
        *s = (*s > 'A' && *s <= 'Z') ? *s+32 : *s;
        s++;
    }
}

//this function turns number in some base to a string
void itoa(int value, char* result, int base)
{
    // check that the base if valid
    if (base < 2 || base > 36) { *result = '\0'; return result; }

    char* ptr = result, *ptr1 = result, tmp_char;
    int tmp_value;

    do {
        tmp_value = value;
        value /= base;
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
    } while ( value );

    // Apply negative sign
    if (tmp_value < 0) *ptr++ = '-';
    *ptr-- = '\0';
    while(ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr--= *ptr1;
        *ptr1++ = tmp_char;
    }
}