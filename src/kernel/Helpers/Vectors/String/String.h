#ifndef STRING_H
#define STRING_H

#define ERROR_CODE 0

typedef struct String
{
    char value;
    struct String* next;
}String;

#include "../../../Helpers/helper.h"
#include "../../../Memory/heap_allocation.h"
#include "../../../drivers/keyboard/keyboard_driver.h"
#include "../../../drivers/screen/screen_driver.h"

String* init_String();
void add_a_char(String* str,char to_add);
void string_append_char_str(String* str,char* to_add);
void string_append(String* str,String* str2);
void print_vector_string(String* str);
void clear_string(String* str);
char string_iterator(String* str,uint32 iterator);
char* string_to_char_array(String* str);
bool compare(String* str1,String* str2);
uint32 string_length(String* str);

#endif