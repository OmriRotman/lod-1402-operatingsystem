#include "String.h"

String* init_String()
{
    String* head = (String*)alloc(sizeof(String));
    head->value = 0;
    head->next = NULL;
}

void add_a_char(String* str,char to_add)
{
    if(str->value == 0)
    {
        str->value = to_add;
    }
    else
    {
        String* current = str;
        while (current->next != NULL) 
        {
            current = current->next;
        }
        current->next = (String*)alloc(sizeof(String));

        current->next->value = to_add;
        current->next->next = NULL;
    }
}

void string_append_char_str(String* str,char* to_add)
{
    int length = get_length(to_add);
    for(int i = 0;i<length;i++)
    {
        add_a_char(str,to_add[i]);
    }
}

void string_append(String* str,String* str2)
{
    int length = string_length(str2);
    for(int i = 0;i<length;i++)
    {
        char to_add = string_iterator(str2,i);
        add_a_char(str,to_add);
    }
}

void print_vector_string(String* str)
{
    String* curr = str;
    while (curr != NULL)
    {
        print_char(curr->value);
        curr = curr->next;
    }
}

void clear_string(String* str)
{
    String* tmp;
    while (str != NULL)
    {
        tmp = str;
        str = str->next;
        free(tmp);
    }
}

char string_iterator(String* str,uint32 iterator)
{
    String* curr = str;
    uint32 len = string_length(str);
    if(iterator > len || iterator < 0) //checking [] range
    {
        print_str("Error: Iterator value is out of range.\n");
        return ERROR_CODE;
    }

    for(int i = 0;i<iterator;i++) //going to the [] in the str
    {
        curr = curr->next;
    }

    return curr->value;
}

char* string_to_char_array(String* str)
{
    uint32 len = string_length(str);
    char* retu = (char*)alloc(sizeof(char)*len); //allocating return value string

    int i = 0;
    String* curr = str;
    while(curr != NULL)
    {
        retu[i] = curr->value;
        i++;
        curr = curr->next;
    }
    retu[len] = 0;

    return retu;
}

bool compare(String* str1,String* str2)
{
    String* tmp1 = str1;
    String* tmp2 = str2;
    while (tmp1 != NULL && tmp2 != NULL)
    {
        if(tmp1->value != tmp2->value)
        {
            return false;
        }
        tmp1 = tmp1->next;
        tmp2 = tmp2->next;
    }
}

uint32 string_length(String* str)
{
    uint32 count = 0;
    String* curr = str;
    while (curr != NULL)
    {
        count++;
        curr = curr->next;
    }

    return count;
}