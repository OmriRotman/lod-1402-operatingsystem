#ifndef VECTORINT_H
#define VECTORINT_H

#define ERROR_CODE 0

#include "../../../Helpers/helper.h"
#include "../../../Memory/heap_allocation.h"
#include "../../../drivers/keyboard/keyboard_driver.h"
#include "../../../drivers/screen/screen_driver.h"

typedef struct VectorInt
{
    uint32 value;
    struct VectorInt* next;
}VectorInt;

VectorInt* init_VectorInt();
void add_a_int(VectorInt* vec,int to_add);
void print_vector_int(VectorInt* vec);
void clear_vector_int(VectorInt* vec);
bool is_in_vector_int(VectorInt* vec,uint32 search);
void erase_from_vector_int(VectorInt* vec,uint32 value);
uint32 vector_int_iterator(VectorInt* vec,uint32 iterator);
uint32 vector_int_length(VectorInt* vec);

#endif