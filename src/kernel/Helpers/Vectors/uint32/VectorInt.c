#include "VectorInt.h"

VectorInt* init_VectorInt()
{
    VectorInt* head = (VectorInt*)alloc(sizeof(VectorInt));
    head->value = 0;
    head->next = NULL;
}

void add_a_int(VectorInt* vec,int to_add)
{
    if(vec->value == NULL)
    {
        vec->value = to_add;
    }
    else
    {
        VectorInt* current = vec;
        while (current->next != NULL) 
        {
            current = current->next;
        }
        current->next = (VectorInt*)alloc(sizeof(VectorInt));

        current->next->value = to_add;
        current->next->next = NULL;
    }
}

void print_vector_int(VectorInt* vec)
{
    VectorInt* curr = vec; //iterating through the list and printing values
    while (curr != NULL)
    {
        print_unsigned_int(curr->value);
        print_char(',');
        curr = curr->next;
    }
    deleteLastVGA(); //delete last ,
    print_char('\n');
}

void clear_vector_int(VectorInt* vec)
{
    VectorInt* tmp;
    while (vec != NULL)
    {
        tmp = vec;
        vec = vec->next;
        free(tmp);
    }
}

bool is_in_vector_int(VectorInt* vec,uint32 search)
{
    VectorInt* curr = vec; //iterating through the list and printing values
    while (curr != NULL)
    {
        print_unsigned_int(curr->value);
        if(curr->value == search)
        {
            return true;
        }
        curr = curr->next;
    }
    return false;
}

void erase_from_vector_int(VectorInt* vec,uint32 value)
{
    VectorInt* temp = vec;
    VectorInt* prev = NULL;

    if (temp != NULL && temp->value == value) //if the head needs to be deleted
    {
        vec = temp->next; // Changed head
        free(temp);  // free old head
        return; //get out of the function
    }
    else //if the node is wherever in the string
    {
        while (temp != NULL && temp->value != value)
        {
            prev = temp;
            temp = temp->next;
        }
        if (temp == NULL) //reached the function end, link was not found
        {
            return; 
        }

        prev->next = temp->next; //removing the link 

        free(temp);
    }
}

uint32 vector_int_iterator(VectorInt* vec,uint32 iterator)
{
    VectorInt* curr = vec;
    uint32 len = vector_int_length(vec);
    if(iterator > len || iterator < 0) //checking [] range
    {
        print_str("Error: Iterator value is out of range.\n");
        return ERROR_CODE;
    }

    for(int i = 0;i<iterator;i++) //going to the [] in the str
    {
        curr = curr->next;
    }

    return curr->value;
}

uint32 vector_int_length(VectorInt* vec)
{
    uint32 retu = 0;
    VectorInt* curr = vec; //iterating through the list and printing values
    while (curr != NULL)
    {
        curr = curr->next;
        retu++;
    }
    return retu;
}