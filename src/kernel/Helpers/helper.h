#ifndef HELPER_H
#define HELPER_H

#include "../drivers/keyboard/keyboard_driver.h"
#include "../drivers/screen/screen_driver.h"
#include "types.h"

void memcpy(uint8 *dest, const uint8 *src, uint32 len);
void memcpy_uint32(uint32 *dest, uint32 *src, uint32 len);
void memset(uint8 *dest, uint8 val, uint32 len);
int strcmp(char *str1, char *str2);
char *strcpy(char *dest, const char *src);
char *strcat(char *dest, const char *src);
void fix_string(char* n, int size);
bool is_between(int start, int finish, int bet);
int get_length(char* a);
int str_cmp(char* a, char* b, int len);
void get_untill(char* input, char* output,char a);
void get_from(char* input, char* output,char a);
void turn_to_capital_letters(char* str);
bool is_small(char a);
int get_int_len(int num);
int pow_10(int num, int p);
void int_to_string(int value, char* result);
void fix_string_to_range_4(char* tofix);
void get_4_byte_string_from_int(int num, char* result);
int atoi(char* str);
int atoi_size(char* str, int size);
void memory_write(char* BUF, int starting_point, char* data, int size);
void memory_read(char* BUF, int starting_point, int length, char* output);
void lower_case(char* str);
void itoa(int value, char* result, int base);
#endif