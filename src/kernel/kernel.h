#ifndef KERNEL_H
#define KERNEL_H

#include "drivers/keyboard/keyboard_driver.h"
#include "drivers/screen/screen_driver.h"
#include "Helpers/types.h"
#include "Userspace/Terminal/assistant.h"
#include "Userspace/Terminal/terminal.h"
#include "Descriptors/descriptor_table.h"
#include "Memory/heap_allocation.h"
#include "FileSystem/FileSystem.h"
#include "Helpers/Vectors/String/String.h"
#include "Helpers/Vectors/uint32/VectorInt.h"
#include "Userspace/Syscalls/Syscalls.h"
#include "Userspace/Bridge/Bridge.h"
#include "MultiThreading/Time/Time.h"

#endif