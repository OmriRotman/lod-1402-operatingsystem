#ifndef HEAP_H
#define HEAP_H

#include "../Helpers/helper.h"

#define KHEAP_START 0xC0000000
#define KHEAP_INITIAL_SIZE  0x100000

#define HEAP_INDEX_SIZE 0x20000
#define HEAP_MAGIC 0x123890AB
#define HEAP_MIN_SIZE 0x70000
#define PAGE_SIZE 0x1000
#define HEAP_PAGE 0x100

void kmalloc_int(uint32 sz, int align, uint32 *phys,uint32* return_poi);
uint32 kmalloc_a(uint32 sz);
uint32 kmalloc_p(uint32 sz, uint32 *phys);
uint32 kmalloc_ap(uint32 sz, uint32 *phys);
uint32 kmalloc(uint32 sz);

uint32 kalloc_at_the_end(uint32 size);

void alloc_block_taken(uint32 location,uint32 size,bool taken);
void alloc_block(uint32 location,uint32 size); //allocates a new block in the struct
void remove_block(uint32 location); //removes a block based on the location (to split it to two)
void free_Block(uint32 location); //frees a block (changes the taken flag)

uint32 find_best_free_block(uint32 size); //finds a block based on its size, returns index in the heap

uint32 alloc(uint32 size); //allocates a new block in the size, returns pointer
void free(uint32 location); //frees a location
void init_heap();

void debug_table();

struct Block
{
    uint32 location;
    uint32 size;
    uint8 ring;
    bool taken;
};
typedef struct Block Block;

#endif 
