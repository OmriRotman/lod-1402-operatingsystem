#include "heap_allocation.h"
#include "paging.h"


// end is defined in the linker script.
extern uint32 end;
uint32 placement_address = (uint32)&end;

static bool heap_flag = false; //if the heap has been firstly allocated
static Block *Heap;
static uint32 Heap_Size = 0; //this is the ammount of blocks written
static uint32 Heap_capacity = HEAP_PAGE; //this is the capacity of the heap block


////////VARS////////
static uint32 virtual_placement_address = KHEAP_START;

////////////////////
/*
this function allocated an array without any checking with the allocation table
input: size to allocate (uint32),align the array to a new page (int,bool), pointer to return physical address to, and pointer to return the virtual pointer to
output:none
*/
void kmalloc_int(uint32 sz, int align, uint32 *phys,uint32* return_poi)
{
    if (align == 1 && (placement_address & 0xFFFFF000) ) //checks if the page address of the placement address is valid
    {
        placement_address &= 0xFFFFF000; //resetting the placement address to the page starting address
        placement_address += 0x1000; //going to the next page
    }
    if (phys)
    {
        *phys = placement_address; //returns physical address
    }
    uint32 tmp = placement_address;
    placement_address += sz;

    *return_poi = tmp; //returning the alloc value
}

/*
this function is a macro that allocats an array,aligned
input:size
output:pointer to virtual value
*/
uint32 kmalloc_a(uint32 sz) //align
{
    uint32 num = 0;
    kmalloc_int(sz, 1, 0,&num);
    return num;
}

/*
this function is a macro that allocats an array
input:size,phys pointer to return physical pointer to
output:pointer to virtual value
*/
uint32 kmalloc_p(uint32 sz, uint32 *phys) //return physical address
{
    uint32 num = 0;
    kmalloc_int(sz, 0, phys,&num);
    return num;
}

/*
this function is a macro that allocats an array, aligned
input:size,phys pointer to return physical pointer to
output:pointer to virtual value
*/
uint32 kmalloc_ap(uint32 sz, uint32 *phys) //align and return physical address
{
    uint32 num = 0;
    kmalloc_int(sz, 1, phys,&num);
    return num;
}

/*
this function mallocs an array in the size given
input:size
output:virtual pointer to the array
*/
uint32 kmalloc(uint32 sz) //do not align and do not return physical address 
{
    uint32 num = 0;
    kmalloc_int(sz, 0, 0,&num);
    return num;
}

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
/*
this function mallocs at the end of the allocated! array space
input:size to allocate
output: pointer to start of the array
*/
uint32 kalloc_at_the_end(uint32 size)
{
    uint32 temp = virtual_placement_address;
    virtual_placement_address += size;
    return temp;
}


/*
this function initiallizes the heap by allocing space for the allocating table
input:none
output:none
*/
void init_heap()
{
    print_str("System info : Initializing the Kernel's heap....\n");
    Heap = kalloc_at_the_end(sizeof(Block)*Heap_capacity); //initializing heap
}

/*
this function allocs a block in the allocating table and reisters it
input:location pointer, size int, taken bool
output:none
*/
void alloc_block_taken(uint32 location,uint32 size,bool taken)//allocates a new block in the struct
{
    if(Heap_Size < Heap_capacity) //heap capacity is fitting, no need to expand the heap
    {
        Block b;
        b.location = location;
        b.size = size;
        b.taken = taken;

        Heap[Heap_Size] = b;
        Heap_Size++;
    }
    else //expand the heap by another heap page size and rewrite it, as well update its location in itself
    {
        uint32 old = Heap;
        uint32 OldCapacity = Heap_capacity;
        Block* new_heap = kalloc_at_the_end(sizeof(Block)*(Heap_capacity + HEAP_PAGE)); //adding another heap page
        memcpy_uint32(new_heap,Heap,Heap_capacity); //copying the heap to the new location
        Heap_capacity += HEAP_PAGE; //updating the new capacity

        Heap = new_heap; //updating the heap pointer
        Block b;
        b.location = location;
        b.size = size;
        b.taken = taken;

        Heap[Heap_Size] = b;
        Heap_Size++;
        alloc_block_taken(old,OldCapacity,false); //adding a block of the old heap to overwrite
    }
    
}

/*
allocated a block at the allocating table
input:uint32 pointer, and uint32 size to alloc
output:none
*/
void alloc_block(uint32 location,uint32 size)//allocates a new block in the struct
{
    alloc_block_taken(location,size,true);
}

/*
this function removes a block in the allocating table
input:deleting by pointer, therefore gets pointer to delete
output:none
*/
void free_Block(uint32 location) //frees a block (changes the taken flag)
{
    for(int i = 0;i<Heap_Size;i++)
    {
        if(Heap[i].location == location)
        {
            Heap[i].taken = false;
            return;
        }
    }
}

/*
this function finds the best free block in the table
input:size
output:pointer to the best fitting block
*/
uint32 find_best_free_block(uint32 size) //finds a block based on its size, returns index in the heap
{
    uint32 index = -1,size_found= 0;
    for(int i = 0; i < Heap_Size; i++)
    {
        if(Heap[i].size >= size && Heap[i].taken == false) //if the size fits and the block is free
        {
            if(size_found != 0) //if theres already a block found. than check if this one is smaller
            {
                if(size_found > Heap[i].size)
                {
                    size_found = Heap[i].size; //writing to the output
                    index = i;
                }
            }
            else //the first fit block
            {
                size_found = Heap[i].size; //writing to the output
                index = i;
            }
        }
    }
    return index;
}

/*
this function is the official allocing function
input:size to allocate (uint32)
output: virtual pointer to the arrays start (uint32)
*/
uint32 alloc(uint32 size) //allocates a new block in the size, returns pointer
{
    int block_index = find_best_free_block(size);

    if(block_index == -1) //no fitting block was found, allocate this in the end
    {
        int address = kalloc_at_the_end(size);
        alloc_block(address,size);
        return address;
    }
    else //a fitting block was found
    {
        if(size == Heap[block_index].size) //if the size EXACTLY fit to the block
        {
            Heap[block_index].taken = true;
            return Heap[block_index].location;
        }
        else //needs to add another entry to the heap allocation table
        {
            int original_size = Heap[block_index].size; //adding another block for the free part (splitting the block)
            alloc_block_taken(Heap[block_index].location + size,original_size - size,false);
            
            Heap[block_index].size = size; //writing the previous block
            Heap[block_index].taken = true;
            return Heap[block_index].location;
        }
    }
}

/*
this function frees memory by just deleting its cell at the allocation table, and so allows to other memory to ovetwrite it
input: pointer to free
output: none
*/
void free(uint32 location) //frees a location
{
    free_Block(location);
}

/*this function prints the heap alloc tab;e top the screen for debugg
input:none
output:none (prints)*/
void debug_table()
{
    print_str("\ndebugging\n");
    for(int i = 0;i<Heap_Size;i++)
    {
        print_str("loc - ");
        print_hex(Heap[i].location);
        print_char('\n');

        print_str("size - ");
        print_unsigned_int(Heap[i].size);
        print_char('\n');

        print_str("taken - ");
        if(Heap[i].taken == true)
        {
            print_str("true");
        }
        else
        {
            print_str("false");
        }
        print_char('\n');
    }
}




