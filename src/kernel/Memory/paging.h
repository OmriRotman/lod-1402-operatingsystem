#ifndef PAGING_H
#define PAGING_H

#include "../Helpers/types.h"
#include "../Helpers/helper.h"
#include "heap_allocation.h"
#include "../Descriptors/isr.h"

typedef struct page // this struct is a page struct
{
    uint32 present    : 1;   // If the page is in memory
    uint32 rw         : 1;   // if it has only read acess
    uint32 user       : 1;   // if it was called by ring 3
    uint32 accessed   : 1;   // if the page has been accessed since the last refresh
    uint32 dirty      : 1;   // if the page has been written to since the last refresh
    uint32 unused     : 7;   // unused - meaningless
    uint32 frame      : 20;  // Frame address (shifted right 12 bits) (doesnt need the others because it is round (followed by zeros))
} page;

typedef struct page_table //struct of pages
{
   page pages[1024];
} page_table;

typedef struct page_directory
{
    page_table *tables[1024]; // Array of pointers to pagetables.
    uint32 tablesPhysical[1024]; //Array of pointers to the pagetables above, but gives their *physical* location, (in order to load into the CR3 register).
    uint32 physicalAddr; //physical address of the page directory
} page_directory;

void initialise_paging();
void switch_page_directory(page_directory *new);
page *get_page(uint32 address, int make, page_directory *dir);
#endif
