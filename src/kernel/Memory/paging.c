#include "paging.h"

page_directory *kernel_directory=0; // The kernel's page directory
page_directory *current_directory=0; // The current page directory;

// A bitset of frames - used or free.
uint32 *frames;
uint32 nframes;

extern uint32 placement_address; //they are defined in the heap implementation - address to the first free adress for block writing , 

#define BITSET_SIZE 32
#define INDEX_FROM_BIT(a) (a/(BITSET_SIZE)) //being used in the bitset implementation (macros)
#define OFFSET_FROM_BIT(a) (a%(BITSET_SIZE))

/*the following section of the code handles with the frame allocation bitset management*/
/*this function sets a frame in the bitset
input:unsigned int (32 bit) frame address to set
output:none (void)*/
static void set_frame(uint32 frame_addr)
{
    uint32 frame = frame_addr/0x1000;
    uint32 idx = INDEX_FROM_BIT(frame);
    uint32 off = OFFSET_FROM_BIT(frame);
    frames[idx] |= (0x1 << off);
}

/*this function clears a frame in the bitset
input:unsigned int (32 bit)
output:none (void)*/
static void clear_frame(uint32 frame_addr)
{
    uint32 frame = frame_addr/0x1000;
    uint32 idx = INDEX_FROM_BIT(frame);
    uint32 off = OFFSET_FROM_BIT(frame);
    frames[idx] &= ~(0x1 << off);
}

/**/
static uint32 test_frame(uint32 frame_addr)
{
    uint32 frame = frame_addr/0x1000;
    uint32 idx = INDEX_FROM_BIT(frame);
    uint32 off = OFFSET_FROM_BIT(frame);
    return (frames[idx] & (0x1 << off));
}

// Static function to find the first free frame.
static uint32 first_frame()
{
    uint32 i, j;
    for (i = 0; i < INDEX_FROM_BIT(nframes); i++)
    {
        if (frames[i] != 0xFFFFFFFF) // nothing free, exit early.
        {
            // at least one bit is free here.
            for (j = 0; j < 32; j++)
            {
                uint32 toTest = 0x1 << j;
                if ( !(frames[i]&toTest) )
                {
                    return i*BITSET_SIZE+j;
                }
            }
        }
    }
}

// Function to allocate a frame. 
void alloc_frame(page *page, int is_kernel, int is_writeable)
{
    if (page->frame != 0)
    {
        return;
    }
    else
    {
        uint32 idx = first_frame();
        if (idx == (uint32)-1) //checking if all frames are taken
        {
            
        }
        set_frame(idx*0x1000);
        page->present = 1;
        page->rw = (is_writeable)?1:0;
        page->user = (is_kernel)?0:1;
        page->frame = idx;
    }
}

// Function to deallocate a frame.
void free_frame(page *page)
{
    uint32 frame;
    if (!(frame=page->frame))
    {
        return;
    }
    else
    {
        clear_frame(frame);
        page->frame = 0x0;
    }
}
///////////////////////////////////////////////////////////////////TILL HERE IS THE BITSET, HAVE TO DOCUMENT IT LATER ON
/*this function initializes the paging by changing control registers and starting up global variables
input:none (void)
output:none (void)*/
void initialise_paging()
{
    uint32 mem_end_page = 0x1000000; //16 MB memory
    
    nframes = mem_end_page / 0x1000; //4KB of memory + (more which is being used)
    frames = (uint32*)kmalloc(INDEX_FROM_BIT(nframes)); //mallocs frames in memory
    memset(frames, 0, INDEX_FROM_BIT(nframes));
    
    kernel_directory = (page_directory*)kmalloc_a(sizeof(page_directory)); //mallocs a page directory
    memset(kernel_directory, 0, sizeof(page_directory)); //sets an empty page directory in the memory
    current_directory = kernel_directory;

    int i = 0;
    for (i = KHEAP_START; i < KHEAP_START+KHEAP_INITIAL_SIZE; i += 0x1000) //in here i go over the memory by addresses and alocates structs of pages each in size of 0x1000 or about 4MB of memory
        get_page(i, 1, kernel_directory);

    i = 0;
    while (i < placement_address+0x1000)
    {
        // Kernel code is readable but not writeable from userspace.
        alloc_frame( get_page(i, 1, kernel_directory), 0, 0);
        i += 0x1000;
    }

    // Now allocate those pages we mapped earlier.
    for (i = KHEAP_START; i < KHEAP_START+KHEAP_INITIAL_SIZE; i += 0x1000)
    {
        alloc_frame( get_page(i, 1, kernel_directory), 0, 0);
    }

    switch_page_directory(kernel_directory); //enable paging switch the directory to our dir
}

/*this function switches page directory by anging the address in CR3 control register
input:a pointer to a page directory struct
output:none*/
void switch_page_directory(page_directory *dir)
{
    current_directory = dir;
    asm volatile("mov %0, %%cr3":: "r"(&dir->tablesPhysical)); //writing physical address to CR3
    uint32 cr0;
    asm volatile("mov %%cr0, %0": "=r"(cr0)); //reads CR0
    cr0 |= 0x80000000; //ORS CR0 value with 1000000... in order to change the PG flag which is at 31 index in the CR0 control flag register
    asm volatile("mov %0, %%cr0":: "r"(cr0)); //writes updated CR0 to memory
}

/*this function gets a memory address and returns it's page
input:adress (unsigned int 32),page directory which the page is in
output:pointer to the page*/
page *get_page(uint32 address, int make, page_directory *dir)  //SHOULD TAKE A SECOND LOOK IN HERE
{
    address /= 0x1000; //turns the address to a page index in memory by dividing by page size
    uint32 table_idx = address / 1024; //finds the page inside the page table

    if (dir->tables[table_idx]) // If this table is already assigned 
    {
        return &dir->tables[table_idx]->pages[address%1024];
    }
    else if(make) //if make page option is on than alloc it
    {
        uint32 tmp;
        dir->tables[table_idx] = (page_table*)kmalloc_ap(sizeof(page_table), &tmp); //reserves space in memory for the page table
        memset(dir->tables[table_idx], 0, 0x1000);
        dir->tablesPhysical[table_idx] = tmp | 0x7; // PRESENT, RW, US.
        return &dir->tables[table_idx]->pages[address%1024];
    }
    else
    {
        return 0;
    }
}

