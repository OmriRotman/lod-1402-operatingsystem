#include "kernel.h"
///////////////////////////////////////////////////////////////////////////
/*this is the kernel's main function, the bootloader loads this function*/
void kernel_entry()
{
  int i = 0;
  print_clear();
  print_set_color(GREEN,BLACK);

  init_gdt(); //initializing descriptor tables
  init_idt();

  initialise_paging();

  init_syscalls_handlers();

  init_heap(); //allocating a heap

  initialize_syscall_params();

  init_fileSystem();
  
  // asm volatile("cli");
  // init_timer(0xfffffff);
  // asm volatile("sti");

  terminal_entry();
 //entering terminal program
  
  return;
}