#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include "../Helpers/helper.h"
#include "../Helpers/types.h"
#include "../Memory/heap_allocation.h"
#include "../drivers/keyboard/keyboard_driver.h"
#include "../drivers/screen/screen_driver.h"
#include "../Helpers/Vectors/String/String.h"
#include "../Helpers/Vectors/uint32/VectorInt.h"

#define NO_FILE_CODE 0
#define EOF 0

struct InodeTable //inode table content (linked list)
{
    char* name; //filename
    uint32 inode; //inode number
    void* file_pointer; //pointer to the file data
    char type; //type of the file 'f' for file 'd' for directory
    uint8 ring_permission; //ring written 0 \ 3
    uint32 fileSize; //file data size in bytes
    struct InodeTable* next;
};
typedef struct InodeTable InodeTable;

void init_fileSystem();
void add_directory_to_system_path(uint32 directory_inode);
void change_directory(char* dirname);
void remove_last_directory_from_path_str();
void make_file(char* filename,uint32 ring);
void make_directory(char* directory_name);
char* read_file(char *name);
void write_file(char* filename,char* data);
char* list_files();
void remove_file(char* name);
void remove_directory(char* name);
void lsof();
char* current_system_directory();

#endif