#include "FileSystem.h"
#include "../Helpers/Vectors/String/String.h"

#define MAX_INT_LENGTH 11
#define SUBSTRACT_THIS_SIZE -1
#define ROOT_INODE 0
#define PATH_PAGE 10

static uint32* current_path;
static uint32 path_capacity = 0;
static uint32 path_size = 0;

static uint32 current_inode = 0;
static InodeTable* inode_table = NULL;
static bool was_FS_initialized = false;

/////some helper funcs

/////////////////////////////////////////
//get last directory inode (the inode of the dir the system path is currently in)
uint32 get_last_directory_from_path()
{
    return current_path[path_size - 1];
}

//this function adds a number to the systems path
void add_directory_to_system_path(uint32 directory_inode)
{
    if(path_size == path_capacity && path_size > path_capacity)
    {
        uint32* vec = (uint32*)alloc(path_capacity + PATH_PAGE);
        path_capacity += PATH_PAGE;
        memcpy_uint32(vec,current_path,path_size);
        free(current_path);
        current_path = vec;
    }
    current_path[path_size] = directory_inode;
    path_size++;
}

//removes the last number (helps to back cd  - [cd..])
void remove_last_directory_from_path_str()
{
    path_size--;
}
////////////////////////////////////////////////////////////////////////////////////////////

//adding file as the head in order to save running time
void add_to_inode_table(char* name,uint32 inode_value ,uint32* file_pointer, char type,uint8 ring,uint32 file_size)
{
    InodeTable* new_f = (InodeTable*)alloc(sizeof(InodeTable));
    new_f->name = name;
    new_f->inode = inode_value;
    new_f->file_pointer = file_pointer;
    new_f->type = type;
    new_f->ring_permission = ring;
    new_f->fileSize = file_size;

    InodeTable* old_head = inode_table;
    inode_table = new_f;
    new_f->next = old_head;
}

//removes a file from the linked list
void remove_from_inode_table(uint32 inode)
{
    InodeTable* temp = inode_table;
    InodeTable* prev = NULL;

    if (temp != NULL && temp->inode == inode) //if the head needs to be deleted
    {
        inode_table = temp->next; // Changed head
        free(temp);  // free old head
        return; //get out of the function
    }
    else //if the node is wherever in the string
    {
        while (temp != NULL && temp->inode != inode)
        {
            prev = temp;
            temp = temp->next;
        }
        if (temp == NULL) //reached the function end, link was not found
        {
            return; 
        }

        prev->next = temp->next; //removing the link 

        free(temp);
    }
}

//searching the linked list by inode and returning the file struct! (recursively)
InodeTable* search_inode_table(InodeTable* curr,uint32 inode_to_search)
{
    if (curr == NULL) //If I reach a null element the list is either empty or over, return null in both cases
    {
        return NULL;
    }
    
    if (curr->inode == inode_to_search) //found the element which is being searched, return it's struct
    {
        return curr;
    }

    return search_inode_table(curr->next, inode_to_search); //recursively calling the same function on the next element in the list
}

//this function starts the FS
void init_fileSystem()
{
    print_str("System info : Initializing File System....\n");
    current_path = (uint32*)alloc(sizeof(uint32)*PATH_PAGE);
    current_path[0] = ROOT_INODE; 
    path_capacity = PATH_PAGE;
    path_size = 1;


    //add root directory
    VectorInt* dir = init_VectorInt();
    add_to_inode_table("root",ROOT_INODE,dir,'d',0,0);
    was_FS_initialized = true;
}

////////////////////////

//(changes all dirs sizes in the path (because the ones who come before in the hirrarchy also change their size))
void change_dir_size(uint32 value,bool increase)
{
    for(int i = 0;i<path_size-1;i++)
    {
        InodeTable* dir = search_inode_table(inode_table,current_path[i]);
        if(increase == true)
        {
            dir->fileSize += value;
        }
        else
        {
            dir->fileSize -= value;
        }
    }
}

//adds directory or file by its inode to the directory inode list
void add_document_to_current_directory(uint32 inode_to_add) //adds file/folder to the current system path directory
{
    uint32 current_directory_inode = get_last_directory_from_path();
    InodeTable* current_directory = search_inode_table(inode_table,current_directory_inode);
    VectorInt* directory_content = (VectorInt*)current_directory->file_pointer;
    add_a_int(directory_content,inode_to_add);
}

//returns inode of file by name inside the directory, if doesn't exist, return NULL
uint32 get_inode_in_current_directory(char* name)
{
    uint32 current_directory_inode = get_last_directory_from_path();
    InodeTable* current_directory = search_inode_table(inode_table,current_directory_inode);
    VectorInt* directory_content = current_directory->file_pointer; //list of inodes of files in directory
    
    while(directory_content != NULL)
    {
        uint32 inode = directory_content->value;
        InodeTable* current_file = search_inode_table(inode_table,inode);

        if(str_cmp(name,current_file->name,get_length(name)) == 0)
        {
            return inode;
        }

        directory_content = directory_content->next;
    }
    return NULL;
}

//make a new empty text file in the current dir
void make_file(char* filename,uint32 ring)
{
    uint32 inode_check = get_inode_in_current_directory(filename); //making sure the file doesnt exist in the directory already
    if(inode_check != NULL)
    {
        print_str("File System Error : ");
        print_str(filename);
        print_str(" already exists in the directory!\n");
        return;
    }
    uint32 inode = ++current_inode;
    add_to_inode_table(filename,inode,0,'f',ring,0);
    add_document_to_current_directory(inode);
}

//makes empty directory in current dir
void make_directory(char* directory_name)
{
    uint32 inode_check = get_inode_in_current_directory(directory_name); //making sure the directory doesnt exist in the directory already
    if(inode_check != NULL)
    {
        print_str("File System Error : ");
        print_str(directory_name);
        print_str(" already exists in the directory!\n");
        return;
    }
    uint32 inode = ++current_inode;
    VectorInt* dir_content = init_VectorInt();
    add_to_inode_table(directory_name,inode,dir_content,'d',0,0);
    add_document_to_current_directory(inode);
}

//reads file by its name from the current directory
char* read_file(char *name)
{
    uint32 inode = get_inode_in_current_directory(name);
    if(inode == NULL)
    {
        print_str("File System Error : File ");
        print_str(name);
        print_str(" does not exist!\n");
        return NULL;
    }

    InodeTable* file = search_inode_table(inode_table,inode);
    return file->file_pointer;
}

//writes this data to a file, removes previous data (TEXT FILE)
void write_file(char* filename,char* data)
{
    uint32 inode = get_inode_in_current_directory(filename);
    if(inode == NULL)
    {
        print_str("File System Error : File ");
        print_str(filename);
        print_str(" does not exist!\n");
        return;
    }
    
    InodeTable* file = search_inode_table(inode_table,inode);
    if(file == NULL)
    {
        print_str("File System Error: File ");
        print_str(filename);
        print_str(" is not present in the inode table!\n");
        return;
    }
    //putting new content in the file pointer and deleting the old content pointer in case it was dynamically allocated
    uint32* old_content_data = file->file_pointer; 
    file->file_pointer = data;
    free(old_content_data);
    uint32 old_size = file->fileSize;
    file->fileSize = get_length(data);
  
    change_dir_size(old_size,false); //substract old size
    change_dir_size(file->fileSize,true); //add new size
}

//ls
char* list_files()
{
    String* to_return = init_String();
    uint32 current_directory_inode = get_last_directory_from_path();
    
    InodeTable* current_directory = search_inode_table(inode_table,current_directory_inode);
    if(current_directory == NULL)
    {
        current_directory = search_inode_table(inode_table,ROOT_INODE);
    }
    VectorInt* directory_content = current_directory->file_pointer; //list of inodes of files in directory
    
    while(directory_content != NULL)
    {
        uint32 inode = directory_content->value;
        if(inode != 0)
        {
            InodeTable* current_file = search_inode_table(inode_table,inode);

            string_append_char_str(to_return,current_file->name);
            string_append_char_str(to_return,",");
        }

        directory_content = directory_content->next;
    }

    char* ret = string_to_char_array(to_return);
    clear_string(to_return);
    return ret;
}

//change directory to the dirname given
void change_directory(char* dirname)
{
    uint32 inode_check = get_inode_in_current_directory(dirname); //making sure the directory exist in the directory already
    if(inode_check == NULL)
    {
        print_str("File System Error : ");
        print_str(dirname);
        print_str(" does not exist in the directory!\n");
        return;
    }
    add_directory_to_system_path(inode_check);
}

void remove_file(char* name)
{
    uint32 current_directory_inode = get_last_directory_from_path();
    uint32 inode = get_inode_in_current_directory(name);
    if(inode == NULL)
    {
        print_str("File System Error : File ");
        print_str(name);
        print_str(" does not exist!\n");
        return;
    }

    InodeTable* file = search_inode_table(inode_table,inode);
    if(file == NULL)
    {
        print_str("File System Error: File ");
        print_str(name);
        print_str(" is not present in the inode table!\n");
        return;
    }

    if(file->type == 'd')
    {
        print_str("File System Error: File ");
        print_str(name);
        print_str(" is a directory! - Try to use rmdir instead..\n");
        return;
    }
    uint32 old_size = file->fileSize;
    InodeTable* directory_its_in = search_inode_table(inode_table,current_directory_inode);
    erase_from_vector_int(directory_its_in->file_pointer,inode);
    change_dir_size(old_size,false);
    free(file->file_pointer); //deleting the allocated strings for the name and the 
    free(file->name);
    remove_from_inode_table(inode);
    free(file);
}

void remove_directory(char* name)
{
    uint32 inode = get_inode_in_current_directory(name);
    if(inode == NULL)
    {
        print_str("File System Error : Directory ");
        print_str(name);
        print_str(" does not exist!\n");
        return;
    }

    InodeTable* file = search_inode_table(inode_table,inode);
    if(file == NULL)
    {
        print_str("File System Error: Directory ");
        print_str(name);
        print_str(" is not present in the inode table!\n");
        return;
    }

    VectorInt* content = (VectorInt*)file->file_pointer;
    while(content != NULL)
    {
        uint32 inode_to_del = content->value;
        InodeTable* to_del = search_inode_table(inode_table,inode_to_del);
        if(to_del->type == 'f')
        {
            remove_file(to_del->name);
        }
        else
        {
            remove_directory(to_del->name);
        }
        content = content->next;
    }

    uint32 current_directory_inode = get_last_directory_from_path();
    InodeTable* directory_its_in = search_inode_table(inode_table,current_directory_inode);
    erase_from_vector_int(directory_its_in->file_pointer,inode);
    remove_from_inode_table(inode);
    clear_vector_int(file->file_pointer);
    free(file->name);
    free(file);
}

//lists every file and directory and gives away it's properties
void lsof()
{
    InodeTable* curr = inode_table;
    char converts[11] =  "";
    
    while(curr != NULL)
    {
        print_str("Inode: ");
        print_unsigned_int(curr->inode);
        
        print_str(" #Name: ");
        print_str(curr->name);
        
        print_str(" #Size: ");
        print_unsigned_int(curr->fileSize);
        

        uint8 ring = curr->ring_permission;
        print_str(" [Bytes] #Permission: ");
        if(ring == 0)
        {
            print_str("Kernel");
        }
        else
        {
            print_str("User");
        }
        print_str(" #Type: ");
        char a = curr->type;
        
        if(curr->type == 'd')
        {
            print_str("Directory");
        }
        else if(curr->type == 'f')
        {
            print_str("File");
        }
        else
        {
            print_str("Unknown");
        }
        print_str("\n");
        curr = curr->next;
    }
}

char* current_system_directory()
{
    String* to_return = init_String();
    string_append_char_str(to_return,"root:/");
    for(int i = 0;i<path_size;i++)
    {
        if(current_path[i] != ROOT_INODE)
        {
            InodeTable* current_dir = search_inode_table(inode_table,current_path[i]);
            if(current_dir != NULL)
            {
                string_append_char_str(to_return,current_dir->name);
                string_append_char_str(to_return,"/");
            }
        }
    }
    return string_to_char_array(to_return);
}